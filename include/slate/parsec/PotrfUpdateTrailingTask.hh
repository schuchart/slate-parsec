#ifndef POTRF_UPDATE_TRAILING_TASK_HH
#define POTRF_UPDATE_TRAILING_TASK_HH

#include <iostream>

#include "slate/parsec/tasks.hh"

namespace parsec {

template<typename matrix_t, typename scalar_t>
class PotrfUpdateColumnTask {
public:

   struct Arguments {
      // Tile row and column index
      int64_t k;
      // Lookahead value
      int64_t j;
      // Matrix pointer
      matrix_t *a;

      // Default constructor
      Arguments() :
         k(-1), j(-1), a(nullptr) { }

      Arguments(int64_t in_k, int64_t in_j, matrix_t *in_a)
         : k(in_k), j(in_j), a(in_a) { }
   };

   void initialize(Arguments const& in_args) {

      this->args_ = Arguments(in_args);
   }

   // Run the task 
   void execute() {
      // TODO: check task has been initialized e.g. a != 0
      this->cpu_func(this->args_.k, this->args_.j, this->args_.a);
   }

   // Submit task
   void submit(parsec_taskpool_t *tp, int priority = 0) {

      // TODO: check task has been initialized e.g. a != 0
      
      auto k = args_.k;
      auto j = args_.j;
      auto a = this->args_.a;

      // Number of row and column tiles
      const int64_t A_nt = a->nt();

      auto *column_k = a->column(k);
      // Update tralining submatrix columns after lookahead ones
      auto *column_j = a->column(j);

      parsec_dtd_taskpool_insert_task(
            tp,
            // parsec_potrf_cpu_func<matrix_t, scalar_t>, // Debug
            parsec_cpu_func,
            priority, /* inherits the priority from high level task */
            "PotrfUpdateColumnTask",
            // We create the dependency to wait for the communication
            sizeof(decltype(k)), &k, VALUE,
            sizeof(decltype(j)), &j, VALUE,
            sizeof(matrix_t *), &a, VALUE,
            PASSED_BY_REF, column_k, INPUT,
            PASSED_BY_REF, column_j, INOUT | AFFINITY,
            PARSEC_DTD_ARG_END);

   }

private:      

   Arguments args_;

   static int parsec_cpu_func(
         parsec_execution_stream_t *es, parsec_task_t *this_task) {
      
      int64_t k;
      int64_t j;
      matrix_t *a;
      int32_t *column_k = nullptr;
      int32_t *column_j = nullptr;

      parsec_dtd_unpack_args(
            this_task, &k, &j, &a, &column_k, &column_j);

      cpu_func(k, j, a);

      // std::cout << "[PotrfUpdateColumnTask::parsec_cpu_func] done" << std::endl;

      return PARSEC_HOOK_RETURN_DONE;
   }

   static void cpu_func(int64_t k, int64_t j, matrix_t *a) {

      // Number of row and column tiles
      const int64_t A_nt = a->nt();

      parsec_taskpool_t *tp = parsec_dtd_taskpool_new();
      a->enqueue_taskpool(tp);

      // Task type
      using UpdateDiagTask = parsec::PotrfUpdateDiagTask<matrix_t, scalar_t>; 
      // using UpdateDiagTask = parsec::PotrfUpdateDiagDataTask<matrix_t, scalar_t>; 
      // Task arguments
      typename UpdateDiagTask::Arguments upd_diag_args(k, j, a);

      UpdateDiagTask upd_diag_task;
      upd_diag_task.initialize(upd_diag_args);
            
      // upd_diag_task.execute(); // Synchronous execution

      upd_diag_task.submit(tp); // Synchronous execution

      //
      // Update sub-diagonal tile in column `j`

      for (int64_t i = j+1; i < A_nt; ++i) {
         // Task type
         // using UpdateTask = parsec::PotrfUpdateDataTask<matrix_t, scalar_t>; 
         using UpdateTask = parsec::PotrfUpdateTask<matrix_t, scalar_t>; 
         // Task arguments
         typename UpdateTask::Arguments upd_args(k, i, j, a);

         UpdateTask upd_task;
         upd_task.initialize(upd_args);

         // upd_task.execute(); // Synchronous execution
         upd_task.submit(tp); // Synchronous execution
      }

      parsec_dtd_taskpool_wait(tp);

      // parsec_dtd_dequeue_taskpool(tp);
      parsec_taskpool_free(tp);
      
   }

};   
   
template<typename matrix_t, typename scalar_t>
class PotrfUpdateTrailingTask {
public:

   struct Arguments {
      // Tile row and column index
      int64_t k;
      // Lookahead value
      int64_t lookahead;
      // Matrix pointer
      matrix_t *a;

      // Default constructor
      Arguments() :
         k(-1), lookahead(-1), a(nullptr) { }

      Arguments(int64_t in_k, int64_t in_lookahead, matrix_t *in_a)
         : k(in_k), lookahead(in_lookahead), a(in_a) { }
   };

   void initialize(Arguments const& in_args) {

      this->args_ = Arguments(in_args);
   }

   // Run the task 
   void execute() {
      // TODO: check task has been initialized e.g. a != 0
      this->cpu_func(this->args_.k, this->args_.lookahead, this->args_.a);
   }

   // Submit task
   void submit(parsec_taskpool_t *tp, int priority = 0) {

      // TODO: check task has been initialized e.g. a != 0
      
      auto k = args_.k;
      auto lookahead = args_.lookahead;
      auto a = this->args_.a;

      // Number of row and column tiles
      const int64_t A_nt = a->nt();

      auto *column_k = a->column(k);
      // Update tralining submatrix columns after lookahead ones
      auto *column_k_plus_one = a->column(k+1+lookahead);
      auto *column_n = a->column(A_nt-1);

      parsec_dtd_taskpool_insert_task(
            tp,
            // parsec_potrf_cpu_func<matrix_t, scalar_t>, // Debug
            parsec_cpu_func,
            priority, /* inherits the priority from high level task */
            "PotrfUpdateTrailingTask",
            // We create the dependency to wait for the communication
            sizeof(decltype(k)), &k, VALUE,
            sizeof(decltype(lookahead)), &lookahead, VALUE,
            sizeof(matrix_t *), &a, VALUE,
            PASSED_BY_REF, column_k, INPUT,
            PASSED_BY_REF, column_k_plus_one, INOUT | AFFINITY,
            PASSED_BY_REF, column_n, INOUT,
            PARSEC_DTD_ARG_END);

   }
   
private:      

   Arguments args_;

   static int parsec_cpu_func(
         parsec_execution_stream_t *es, parsec_task_t *this_task) {
      
      int64_t k;
      int64_t lookahead;
      matrix_t *a;
      int32_t *column_k = nullptr;
      int32_t *column_k_plus_one = nullptr;
      int32_t *column_n = nullptr;

      parsec_dtd_unpack_args(
            this_task, &k, &lookahead, &a, &column_k, &column_k_plus_one, &column_n);

      cpu_func(k, lookahead, a);

      // std::cout << "[PotrfUpdateTrailingTask::parsec_cpu_func] done" << std::endl;

      return PARSEC_HOOK_RETURN_DONE;
   }

   static void cpu_func(int64_t k, int64_t lookahead, matrix_t *a) {

      // Number of row and column tiles
      const int64_t A_nt = a->nt();

      parsec_taskpool_t *tp = parsec_dtd_taskpool_new();
      // parsec_taskpool_t *tp = a->parsec_potrf_panel_tp;
      a->enqueue_taskpool(tp);

      for (int64_t j = k+1+lookahead; j < A_nt; ++j) {

         //
         // Update diagonal tile in column `j`

         // Debug
         // if (0 == A.mpiRank()) {
         //    sleep(1);
         // }
            
         // Task type
         // using UpdateDiagTask = parsec::PotrfUpdateDiagDataTask<matrix_t, scalar_t>; 
         using UpdateDiagTask = parsec::PotrfUpdateDiagTask<matrix_t, scalar_t>; 
         // Task arguments
         typename UpdateDiagTask::Arguments upd_diag_args(k, j, a);

         UpdateDiagTask upd_diag_task;
         upd_diag_task.initialize(upd_diag_args);
            
         // upd_diag_task.execute(); // Synchronous execution

         upd_diag_task.submit(tp); // Synchronous execution
         // parsec_dtd_taskpool_wait(A.parsec_context, tp);

         //
         // Update sub-diagonal tile in column `j`

         for (int64_t i = j+1; i < A_nt; ++i) {

            // Task type
            using UpdateTask = parsec::PotrfUpdateTask<matrix_t, scalar_t>; 
            // using UpdateTask = parsec::PotrfUpdateDataTask<matrix_t, scalar_t>; 
            // Task arguments
            typename UpdateTask::Arguments upd_args(k, i, j, a);

            UpdateTask upd_task;
            upd_task.initialize(upd_args);

            // upd_task.execute(); // Synchronous execution
            upd_task.submit(tp); // Synchronous execution
  
         }
      }

      parsec_dtd_taskpool_wait(tp);

      // parsec_dtd_dequeue_taskpool(tp);
      parsec_taskpool_free(tp);
      
   }
   
};
   
}

#endif POTRF_UPDATE_TRAILING_TASK_HH
