#ifndef POTRF_TASK_HH
#define POTRF_TASK_HH

#include <iostream>

#include "slate/parsec/tasks.hh"

namespace parsec {

   // Potrf Parsec task with data dependency tracking
   //

template<typename matrix_t, typename scalar_t>
class PotrfDataTask {
public:

   PotrfDataTask() { }
      
   struct Arguments {
      // Tile row and column index
      int64_t k;
      // Matrix pointer
      matrix_t *a;
      // Pull in data from GPU device
      bool pullin;

      // Default constructor
      Arguments() :
         k(-1), a(nullptr), pullin(false) { }

      Arguments(int64_t in_k, matrix_t *in_a)
         : k(in_k), a(in_a), pullin(false) { }
   };

   // Run the task 
   void execute() {
      // TODO: check task has been initialized e.g. a != 0
      this->cpu_func(this->args_.k, this->args_.a, nullptr);
   }

   void initialize(Arguments const& in_args) {

      this->args_ = Arguments(in_args);
   }

   // Submit task
   void submit(parsec_taskpool_t *tp, int priority = 0) {

      // TODO: check task has been initialized e.g. a != 0

      auto *a = args_.a;
      auto k = args_.k;

      // Retrieve rank owning tile a(k, k)
      int tile_rank = this->args_.a->tileRank(k, k);

      auto* akk_dtd_tile = a->desc->dtd_tiles[a->desc->super.data_key(&(a->desc->super), k, k)];
      int akk_arena_index = -1;
      // int akk_arena_index = (a->tileRank(k, k) == a->mpiRank()) ? parsec::LAPACK_TILE : parsec::FULL_TILE;
      akk_arena_index = get_arena_index(a, k, k);

      int akk_op_type = INOUT | akk_arena_index;

#ifdef PARSEC_DTD_HAVE_CUDA
      if (this->args_.pullin){
         // Indicate that we are pulling data from GPU device
         akk_op_type |= PULLIN;
      }
#endif

      // std::cout << "[PotrfTask::submit] A(" << k << ", " << k <<  ") stride = " <<  << std::endl;

      parsec_dtd_taskpool_insert_task(
            tp,
            parsec_cpu_func,
            priority,
            "PotrfTask",
            sizeof(decltype(k)), &k, VALUE,
            sizeof(matrix_t *), &args_.a, VALUE,
            PASSED_BY_REF, akk_dtd_tile, akk_op_type,
            // Execute on node owning a(k, k) tile
            sizeof(int), &tile_rank, VALUE | AFFINITY,
            PARSEC_DTD_ARG_END);
   }

private:      

   Arguments args_;
 
private:      
   // public: // Debug
     
   static void cpu_func(int64_t k, matrix_t *a, scalar_t *akk_data) {

      if (a->tileIsLocal(k, k)) {

         // std::cout << "[PotrfDataTask::cpu_func] A(" << k << ", " << k <<  ")" << std::endl;

         auto akk = a->at(k, k);

         // std::cout << "[PotrfTask::cpu_func] A(" << k << ", " << k <<  ") stride = "
         //           << akk.stride()  << std::endl;

         slate::potrf<scalar_t>(akk);

      }
   }

   static int parsec_cpu_func(
         parsec_execution_stream_t *es, parsec_task_t *this_task) {

      (void)es;
         
      int64_t k;
      matrix_t *a;
      scalar_t *akk_data;
      int tile_rank;
         
      parsec_dtd_unpack_args(this_task, &k, &a, &akk_data, &tile_rank);

// #ifdef PARSEC_DTD_HAVE_CUDA
//       std::cout << "[PotrDatafTask::parsec_cpu_func] A(" << k << ", " << k <<  ")" << std::endl;
// #endif

// #ifdef PARSEC_DTD_HAVE_CUDA
//       printf("[PotrfDataTask::parsec_cpu_func] akk = %.12f\n", akk_data[0]);
// #endif
      
      cpu_func(k, a, akk_data);

// #ifdef PARSEC_DTD_HAVE_CUDA
//       printf("[PotrfDataTask::parsec_cpu_func] lkk = %.12f\n", akk_data[0]);
// #endif
      
      return PARSEC_HOOK_RETURN_DONE;      
   }

};
   
   //
   // Potrf Parsec task
   //


   ////
   // Debug
   
   // template<typename matrix_t, typename scalar_t>
   // class PotrfTask;
   
   // template<typename matrix_t, typename scalar_t>
   // int parsec_potrf_cpu_func(
   //       parsec_execution_stream_t *es, parsec_task_t *this_task) {
      
   //    (void)es;
         
   //    int64_t k;
   //    matrix_t *a;
   //    int tile_rank;
      
   //    parsec_dtd_unpack_args(this_task, &k, &a, &tile_rank);

   //    std::cout << "[parsec_potrf_cpu_func] A(" << k << ", " << k <<  ")" << std::endl;

   //    PotrfTask<matrix_t, scalar_t>::cpu_func(k, a);
         
   //    return PARSEC_HOOK_RETURN_DONE;      
   // }

   // End debug
   ////
   
template<typename matrix_t, typename scalar_t>
class PotrfTask {
public:
      
   PotrfTask() { }
      
   struct Arguments {
      // Tile row and column index
      int64_t k;
      // Matrix pointer
      matrix_t *a;

      // Default constructor
      Arguments() :
         k(-1), a(nullptr) { }

      Arguments(int64_t in_k, matrix_t *in_a)
         : k(in_k), a(in_a) { }
   };

   void initialize(Arguments const& in_args) {

      this->args_ = Arguments(in_args);
   }
      
   // Run the task 
   void execute() {
      // TODO: check task has been initialized e.g. a != 0
      this->cpu_func(this->args_.k, this->args_.a);
   }

   // Submit task
   void submit(parsec_taskpool_t *tp, int priority = 0) {

      // TODO: check task has been initialized e.g. a != 0

      auto k = args_.k;

      // Retrieve rank owning tile a(k, k)
      int tile_rank = this->args_.a->tileRank(k, k);

      // std::cout << "[PotrfTask::submit] A(" << k << ", " << k <<  ")" << std::endl;

      parsec_dtd_taskpool_insert_task(
            tp,
            // parsec_potrf_cpu_func<matrix_t, scalar_t>, // Debug
            parsec_cpu_func,
            priority, /* inherits the priority from high level task */
            "PotrfTask",
            // We create the dependency to wait for the communication
            sizeof(decltype(k)), &k, VALUE,
            sizeof(matrix_t *), &args_.a, VALUE,
            // Execute on node owning a(k, k) tile
            sizeof(int), &tile_rank, VALUE | AFFINITY,
            PARSEC_DTD_ARG_END);

   }
      
private:      

   Arguments args_;
 
private:      
   // public: // Debug
     
   static void cpu_func(int64_t k, matrix_t *a) {

      if (a->tileIsLocal(k, k)) {

         // std::cout << "[PotrfTask::cpu_func] A(" << k << ", " << k <<  ")" << std::endl;

         auto akk = a->at(k, k);
            
         slate::potrf<scalar_t>(akk);
      }
   }

   static int parsec_cpu_func(
         parsec_execution_stream_t *es, parsec_task_t *this_task) {

      (void)es;
         
      int64_t k;
      matrix_t *a;
      int tile_rank;
         
      parsec_dtd_unpack_args(this_task, &k, &a, &tile_rank);

      // std::cout << "[PotrfTask::parsec_cpu_func] A(" << k << ", " << k <<  ")" << std::endl;
      
      cpu_func(k, a);
         
      return PARSEC_HOOK_RETURN_DONE;      
   }
      
};

}

#endif POTRF_TASK_HH
