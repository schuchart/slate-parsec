#ifndef PARSEC_HH
#define PARSEC_HH

#include "slate/internal/mpi.hh"

#include "parsec.h"
#include "parsec/mca/device/device.h"

#include <chrono>
#include <map>
#include <complex>
#include <vector>
#include <exception>

#if defined(PARSEC_HAVE_CUDA)
#include <cublas_v2.h>
#endif

namespace parsec {

#if defined(PARSEC_HAVE_CUDA)

extern std::map<cudaStream_t, cublasHandle_t> cublas_handles;

cublasHandle_t get_cublas_handle(cudaStream_t custream);   

// extern parsec_info_id_t CuHI;
   
// cublasHandle_t create_or_lookup_cublas_handle(parsec_device_module_t *mod,
//                                               parsec_info_id_t cuhi,
//                                               cudaStream_t cuda_stream);

int get_nb_cuda_devices();

std::vector<int> get_cuda_dev_index();

template<typename matrix_t>   
int get_tile_device_index(
      matrix_t *a, int i, int j/*, std::vector<int> cuda_dev_index*/) {

   int num_devices = a->parsec_cuda_dev_index.size();
   int dev_index = -1;

   // return a->parsec_cuda_dev_index[1]; // DEBUG
   // return (j == (a->nt()-1)) ? a->parsec_cuda_dev_index[1] : a->parsec_cuda_dev_index[0]; // DEBUG
   
   if (num_devices > 0) {
      int i = -1;
      int q = a->desc->q;

      // Distribute local matrix in 1D column block cyclic fashion
      // among devices.
      i = int(j/q)%num_devices;
      dev_index = a->parsec_cuda_dev_index[i];
   }

   return dev_index;
}

////////////////////////////////

   
inline cublasStatus_t cublasTrsm(
      cublasHandle_t handle,
      cublasSideMode_t side,
      cublasFillMode_t uplo,
      cublasOperation_t trans,
      cublasDiagType_t  diag,
      int m,
      int n,
      const float* alpha,  /* host or device pointer */
      const float* A, int lda,
      float* B, int ldb) {
   
   return cublasStrsm(
         handle, side, uplo, trans, diag,
         m, n, alpha,
         A, lda,
         B, ldb );

}
   
inline cublasStatus_t cublasTrsm(
      cublasHandle_t handle,
      cublasSideMode_t side,
      cublasFillMode_t uplo,
      cublasOperation_t trans,
      cublasDiagType_t  diag,
      int m,
      int n,
      const double* alpha,  /* host or device pointer */
      const double* A, int lda,
      double* B, int ldb) {
   
   return cublasDtrsm(
         handle, side, uplo, trans, diag,
         m, n,
         alpha,
         A, lda,
         B, ldb );

}

inline cublasStatus_t cublasTrsm(
      cublasHandle_t handle,
      cublasSideMode_t side,
      cublasFillMode_t uplo,
      cublasOperation_t trans,
      cublasDiagType_t  diag,
      int m,
      int n,
      const std::complex<float>* alpha,  /* host or device pointer */
      const std::complex<float>* A, int lda,
      std::complex<float>* B, int ldb) {
   
   return cublasCtrsm(
         handle, side, uplo, trans, diag,
         m, n,
         (cuComplex*)alpha,
         (cuComplex const*)A, lda,
         (cuComplex*)B, ldb );

}

inline cublasStatus_t cublasTrsm(
      cublasHandle_t handle,
      cublasSideMode_t side,
      cublasFillMode_t uplo,
      cublasOperation_t trans,
      cublasDiagType_t  diag,
      int m,
      int n,
      const std::complex<double>* alpha,  /* host or device pointer */
      const std::complex<double>* A, int lda,
      std::complex<double>* B, int ldb) {
   
   return cublasZtrsm(
         handle, side, uplo, trans, diag,
         m, n,
         (cuDoubleComplex*) alpha,
         (cuDoubleComplex const*)A, lda,
         (cuDoubleComplex*)B, ldb);

}
   
////////////////////////////////

inline cublasStatus_t cublasGemmEx(
    cublasHandle_t handle,
    cublasOperation_t transa,
    cublasOperation_t transb,
    int m,
    int n,
    int k,
    const float* alpha,  /* host or device pointer */
    const float* A,
    int lda,
    const float* B,
    int ldb,
    const float* beta,   /* host or device pointer */
    float* C,
    int ldc)
{

   return cublasGemmEx(
         handle, transa, transb,
         m, n, k,
         alpha,
         A, CUDA_R_32F, lda,
         B, CUDA_R_32F, ldb,
         beta,
         C, CUDA_R_32F, ldc,
         CUDA_R_32F,
         // CUBLAS_GEMM_DEFAULT
         CUBLAS_GEMM_DEFAULT_TENSOR_OP
         );
}
   
inline cublasStatus_t cublasGemmEx(
    cublasHandle_t handle,
    cublasOperation_t transa,
    cublasOperation_t transb,
    int m,
    int n,
    int k,
    const double* alpha,  /* host or device pointer */
    const double* A,
    int lda,
    const double* B,
    int ldb,
    const double* beta,   /* host or device pointer */
    double* C,
    int ldc)
{
   throw std::runtime_error("Not supported");
   return CUBLAS_STATUS_SUCCESS;
}


inline cublasStatus_t cublasGemmEx(
    cublasHandle_t handle,
    cublasOperation_t transa,
    cublasOperation_t transb,
    int m,
    int n,
    int k,
    const std::complex<float>* alpha,  /* host or device pointer */
    const std::complex<float>* A,
    int lda,
    const std::complex<float>* B,
    int ldb,
    const std::complex<float>* beta,   /* host or device pointer */
    std::complex<float>* C,
    int ldc)
{
   throw std::runtime_error("Not supported");
   return CUBLAS_STATUS_SUCCESS;
}

inline cublasStatus_t cublasGemmEx(
    cublasHandle_t handle,
    cublasOperation_t transa,
    cublasOperation_t transb,
    int m,
    int n,
    int k,
    const std::complex<double>* alpha,  /* host or device pointer */
    const std::complex<double>* A,
    int lda,
    const std::complex<double>* B,
    int ldb,
    const std::complex<double>* beta,   /* host or device pointer */
    std::complex<double>* C,
    int ldc)
{
   throw std::runtime_error("Not supported");
   return CUBLAS_STATUS_SUCCESS;
}
   
////////////////////////////////

inline cublasStatus_t cublasGemm(
    cublasHandle_t handle,
    cublasOperation_t transa,
    cublasOperation_t transb,
    int m,
    int n,
    int k,
    const float* alpha,  /* host or device pointer */
    const float* A,
    int lda,
    const float* B,
    int ldb,
    const float* beta,   /* host or device pointer */
    float* C,
    int ldc)
{
    return cublasSgemm(handle, transa, transb, m, n, k,
                       alpha, A, lda,
                              B, ldb,
                       beta,  C, ldc);
}
   
inline cublasStatus_t cublasGemm(
    cublasHandle_t handle,
    cublasOperation_t transa,
    cublasOperation_t transb,
    int m,
    int n,
    int k,
    const double* alpha,  /* host or device pointer */
    const double* A,
    int lda,
    const double* B,
    int ldb,
    const double* beta,   /* host or device pointer */
    double* C,
    int ldc)
{
    return cublasDgemm(handle, transa, transb, m, n, k,
                       alpha, A, lda,
                              B, ldb,
                       beta,  C, ldc);
}

inline cublasStatus_t cublasGemm(
    cublasHandle_t handle,
    cublasOperation_t transa,
    cublasOperation_t transb,
    int m,
    int n,
    int k,
    const std::complex<float>* alpha,  /* host or device pointer */
    const std::complex<float>* A,
    int lda,
    const std::complex<float>* B,
    int ldb,
    const std::complex<float>* beta,   /* host or device pointer */
    std::complex<float>* C,
    int ldc)
{
    return cublasCgemm(handle, transa, transb, m, n, k,
                       (cuComplex*)  alpha,
                       (const cuComplex*) A, lda,
                       (const cuComplex*) B, ldb,
                       (cuComplex*)  beta,
                       (cuComplex*) C, ldc);
}

inline cublasStatus_t cublasGemm(
    cublasHandle_t handle,
    cublasOperation_t transa,
    cublasOperation_t transb,
    int m,
    int n,
    int k,
    const std::complex<double>* alpha,  /* host or device pointer */
    const std::complex<double>* A,
    int lda,
    const std::complex<double>* B,
    int ldb,
    const std::complex<double>* beta,   /* host or device pointer */
    std::complex<double>* C,
    int ldc)
{
    return cublasZgemm(handle, transa, transb, m, n, k,
                       (cuDoubleComplex*)  alpha,
                       (const cuDoubleComplex*) A, lda,
                       (const cuDoubleComplex*) B, ldb,
                       (cuDoubleComplex*)  beta,
                       (cuDoubleComplex*) C, ldc);
}
   
#endif
   
class Taskpool {
public:
   Taskpool()
      : tp_(parsec_dtd_taskpool_new())
   {
      assert(nullptr != tp_);
   }
   
   Taskpool(parsec_taskpool_t *in_tp)
      : tp_(in_tp)
   {
   }

   parsec_taskpool_t * taskpool_t() {
      return this->tp_;
   }

   // Wait for the completion of all the tasks inserted to this
   // taskpool
   void wait() {
      parsec_dtd_taskpool_wait(tp_);      
   }
   
private:
   parsec_taskpool_t *tp_;   
};   
   
}

#endif // PARSEC_HH
