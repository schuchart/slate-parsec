
#ifndef PARSEC_MATRIX_WRAPPER_HH
#define PARSEC_MATRIX_WRAPPER_HH

#include "slate/internal/mpi.hh"

#include <complex>
#include <iostream>

#include "parsec.h"
// #include "parsec/parsec_config.h"
// #include "parsec/parsec_internal.h"
// #include "parsec/data.h"
#include "parsec/data_internal.h"
#include "parsec/data_dist/matrix/matrix.h"
// #include "parsec/scheduling.h"
// #include "parsec/interfaces/superscalar/insert_function.h"
#include "parsec/interfaces/superscalar/insert_function_internal.h"
// #include "parsec/execution_stream.h"
// #include "parsec/parsec_description_structures.h"
// #include "parsec/data_dist/matrix/two_dim_rectangle_cyclic.h"
// #include "parsec/remote_dep.h"
// #include "parsec/mca/device/device.h"

// // namespace MPI {

// //     template<class B, typename T>
// //     class A {
// //         static MPI_Datatype type() {
// //             return B::type();
// //         }
// //     };

// //     template<typename T>
// //     class Wrapper : public A< Wrapper<T>, T > {
// //     public:
// //         static MPI_Datatype datatype() {
// //             return MPI_INTEGER;
// //         }
// //     };

// //     template <> MPI_Datatype Wrapper<                float >::datatype();
// //     template <> MPI_Datatype Wrapper<               double >::datatype();
// //     template <> MPI_Datatype Wrapper<  std::complex<float> >::datatype();
// //     template <> MPI_Datatype Wrapper< std::complex<double> >::datatype();

// //     template <> MPI_Datatype Wrapper<                 char >::datatype();
// //     template <> MPI_Datatype Wrapper<        unsigned char >::datatype();
// //     template <> MPI_Datatype Wrapper<               int8_t >::datatype();
// //     template <> MPI_Datatype Wrapper<     signed short int >::datatype();
// //     template <> MPI_Datatype Wrapper<   unsigned short int >::datatype();
// //     template <> MPI_Datatype Wrapper<           signed int >::datatype();
// //     template <> MPI_Datatype Wrapper<         unsigned int >::datatype();
// //     template <> MPI_Datatype Wrapper<        long long int >::datatype();
// //     template <> MPI_Datatype Wrapper<      signed long int >::datatype();

// //     /*
// //       Use it this way:
// //       std::cout << "Type of float: " <<   (int64_t) MPI::Wrapper<float>::datatype() << std::endl;
// //       std::cout << "Type of double: " <<  (int64_t) MPI::Wrapper<double>::datatype() << std::endl;
// //       std::cout << "Type of int32_t: " << (int64_t) MPI::Wrapper<int32_t>::datatype() << std::endl;
// //       std::cout << "Type of int64_t: " << (int64_t) MPI::Wrapper<int64_t>::datatype() << std::endl;
// //     */

// // } // End of namespace MPI

namespace parsec {

   enum format {
                LAPACK_TILE = 1,
                FULL_TILE = 2,
                LAPACK_LAST_ROW = 3,
                LAPACK_LAST_COL = 4,
                LAPACK_LAST_ROW_COL = 5,
                FULL_LAST_ROW = 6,
                FULL_LAST_COL = 7,
                FULL_LAST_ROW_COL = 8,
   };
   

   template<typename matrix_t>
   int get_arena_index(matrix_t *a, int64_t i, int64_t j) {

      int arena_index = -1;

      if (a->tileIsLocal(i, j)) {

         auto aij = a->at(i, j);

         auto mb = aij.mb(); 
         auto nb = aij.nb(); 
         auto stride = aij.stride();

         bool is_last_row = (i == (a->mt()-1)); 
         bool is_last_col = (j == (a->nt()-1)); 
         
         if (stride > mb) {
            // Lapack layout
            arena_index = parsec::LAPACK_TILE;
         }
         else {
            // Tile layout
            arena_index = parsec::FULL_TILE;

            if (is_last_row) {
               // Last row tile

               if (is_last_col) {
                  arena_index = parsec::FULL_LAST_ROW_COL;
               }
               else {
                  arena_index = parsec::FULL_LAST_ROW;
               }
            }
            else if(is_last_col) {
               // Last column tile

               arena_index = parsec::FULL_LAST_COL;
            }
         }
      }
      else {
         arena_index = parsec::FULL_TILE;
      }

      
      return arena_index;
   }
   
   
   int slate_parsec_complete_tp_callback(parsec_taskpool_t* tp, void* cb_data);
   // int slate_parsec_complete_lookahead_tp_callback(parsec_taskpool_t* tp, void* cb_data);

   /*
     PaRSEC data_collection that will express dependencies between
     high level tasks.  Each data is fake but represents a column of
     the entry matrix.  High level operations are performed on column
     or set of column at any given step of the algorithm.
   */

   typedef struct parsec_column_s {
      parsec_data_collection_t super;
      int32_t *array;
      parsec_data_t **data;
      int64_t nt;
   } parsec_column_t;

   void parsec_column_init(parsec_column_t **holder, int64_t nt, int my_rank);
   void parsec_column_fini(parsec_column_t **desc);

   uint32_t       parsec_two_dim_block_rank_of(parsec_data_collection_t* dc, ...);
   int32_t        parsec_two_dim_block_vpid_of(parsec_data_collection_t* dc, ...);
   parsec_data_t* parsec_two_dim_block_data_of(parsec_data_collection_t* dc, ...);
   uint32_t       parsec_two_dim_block_rank_of_key(parsec_data_collection_t* dc, parsec_data_key_t key);
   int32_t        parsec_two_dim_block_vpid_of_key(parsec_data_collection_t* dc, parsec_data_key_t key);
   parsec_data_t* parsec_two_dim_block_data_of_key(parsec_data_collection_t* dc, parsec_data_key_t key);
   parsec_key_t   parsec_two_dim_block_data_key(parsec_data_collection_t *dc, ...);
   void           parsec_two_dim_block_key_to_coordinates(parsec_data_collection_t *desc, parsec_data_key_t key, int64_t *i, int64_t *j);
   int            parsec_two_dim_block_key_to_string(struct parsec_data_collection_s* desc, parsec_data_key_t datakey, char * buffer, uint32_t buffer_size);

   typedef struct parsec_two_dim_block_s {
      parsec_data_collection_t   super;
      parsec_data_t            **data_map;
      parsec_dtd_tile_t        **dtd_tiles;
      int64_t                    lda;
      uint32_t                   mt, nt;
      uint32_t                   mb, nb;
      uint32_t                   p, q;
   } parsec_two_dim_block_t;

   template<typename FloatType>
      void parsec_two_dim_block_init(
            parsec_two_dim_block_t **holder,
            int nodes,    int myrank,
            int mb,       int nb,   /* Tile size */
            int64_t lm,   int64_t ln,   /* Global matrix size (what is stored)*/
            int64_t i,    int64_t j,    /* Staring point in the global matrix */
            int64_t m,    int64_t n,    /* Submatrix size (the one concerned by the computation */
            int64_t lda,                /* Leading dimension of global matrix */
            int nrst,     int ncst,     /* Super-tiling size */
            int64_t p,    int64_t q)
   {
      parsec_two_dim_block_t *desc = (parsec_two_dim_block_t*)calloc(1, sizeof(parsec_two_dim_block_t));

      parsec_data_collection_t *o = (parsec_data_collection_t*)(desc);
      parsec_data_collection_init( o, nodes, myrank );
      parsec_dtd_data_collection_init(o);

      o->rank_of      = parsec_two_dim_block_rank_of;
      o->vpid_of      = parsec_two_dim_block_vpid_of;
      o->data_of      = parsec_two_dim_block_data_of;
      o->rank_of_key  = parsec_two_dim_block_rank_of_key;
      o->vpid_of_key  = parsec_two_dim_block_vpid_of_key;
      o->data_of_key  = parsec_two_dim_block_data_of_key;
      o->data_key     = parsec_two_dim_block_data_key;
      o->key_to_string = parsec_two_dim_block_key_to_string;

      desc->nt  = n % nb == 0 ? n/nb : n/nb+1;
      desc->mt  = m % nb == 0 ? m/nb : m/nb+1;
      desc->nb  = nb;
      desc->mb  = mb;
      desc->lda = lda;
      desc->p   = p;
      desc->q   = q;

      desc->data_map  = new parsec_data_t*[ desc->nt * desc->mt ];
      desc->dtd_tiles = new parsec_dtd_tile_t*[ desc->nt * desc->mt ];

      for (int64_t c = 0; c < desc->nt * desc->mt; ++c) {
         desc->data_map[c] = NULL;
         desc->dtd_tiles[c] = NULL;
      }

      *holder = desc;
   }

   template<typename FloatType>
      void parsec_two_dim_block_fini(parsec_two_dim_block_t *desc) {
      assert(NULL != desc);
      parsec_dtd_data_collection_fini((parsec_data_collection_t *)desc);
      parsec_data_collection_destroy( (parsec_data_collection_t*)desc );
      // parsec_tiled_matrix_dc_destroy((parsec_tiled_matrix_dc_t *)desc);
       
      // there are some frees to do
      desc = NULL;
   }

   // matrix is in scalapack format, we have to access the memory region
   // for each tile, if it's local, store a pointer to the beginning and

   template<typename scalar_t>
   parsec_data_t*
   tile_data_create(
         parsec_two_dim_block_t *desc,
         int64_t i, int64_t j,
         void *tile, void *ptr,
         int64_t mb, int64_t nb) {
      
      parsec_data_t *data = NULL;
      
      assert(NULL != desc);
      assert((i < desc->mt) && (j < desc->nt));

      // std::cout << "Creating tile ("<<i<<","<<j<<"), size: "<<mb<<"x"<<nb<<", ptr = " << ptr << std::endl;
      data = PARSEC_OBJ_NEW(parsec_data_t);
      data->owner_device = 0;
      data->key = desc->super.data_key((parsec_data_collection_t*)desc, i, j);
      data->dc = (parsec_data_collection_t*)desc;
      data->nb_elts = mb*nb*sizeof(scalar_t);

      parsec_data_copy_t* data_copy = (parsec_data_copy_t*)PARSEC_OBJ_NEW(parsec_data_copy_t);

      data_copy->older = NULL;
      data_copy->coherency_state = PARSEC_DATA_COHERENCY_OWNED;
      data_copy->device_private = (void*)ptr;

      int rc = parsec_data_copy_attach(data, data_copy, 0);
      assert(PARSEC_SUCCESS == rc);
      // data_copy->device_index    = 0;
      // data_copy->original        = data;
      // /* Atomically set the device copy */
      // data_copy->older = data->device_copies[0];
      // data->device_copies[0] = data_copy;
      // PARSEC_OBJ_RETAIN(data);
      
      desc->data_map[data->key] = data;
      desc->dtd_tiles[data->key] = parsec_dtd_tile_of(&(desc->super), data->key);

      return data;
   }

   template<typename scalar_t>
      void tile_data_destroy(
            parsec_two_dim_block_t *desc, int64_t i, int64_t j)
   {

      assert(NULL != desc);
      assert((i < desc->mt) && (j < desc->nt));

      // std::cout << "Destroy tile ("<<i<<";"<<j<<")" << std::endl;

      parsec_data_key_t key = desc->super.data_key((parsec_data_collection_t*)desc, i, j);
      // FIXME: defined in insert_function_internal.h not installed
      // by default with Parsec
      assert(key < (desc->nt * desc->mt));

      // parsec_dtd_tile_remove((parsec_data_collection_t*)desc, desc->dtd_tiles[key]);
      parsec_dtd_tile_remove((parsec_data_collection_t*)desc, key);

      // parsec_hash_table_t *hash_table = (parsec_hash_table_t *)(((parsec_data_collection_t*)desc)->tile_h_table);
      // parsec_hash_table_remove( hash_table, (parsec_key_t)key );

   }


   template<typename scalar_t>
      void tile_data_register(
            parsec_two_dim_block_t *desc,
            int64_t i, int64_t j,
            scalar_t *ptr)
   {

      assert(NULL != desc);
      assert((i < desc->mt) && (j < desc->nt));
      assert(NULL != ptr);
 
      // std::cout << "Register tile ("<<i<<";"<<j<<") > ptr = " << ptr << std::endl;

      parsec_data_t* data = desc->super.data_of((parsec_data_collection_t*)desc, i, j);
      data->device_copies[0]->device_private = (void*)ptr;
   }
} // End of namespace parsec

#endif //PARSEC_MATRIX_WRAPPER_HH
