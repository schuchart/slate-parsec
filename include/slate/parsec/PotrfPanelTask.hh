#ifndef POTRF_PANEL_TASK_HH
#define POTRF_PANEL_TASK_HH

#include <iostream>

#include "slate/parsec/PotrfTask.hh"
#include "slate/parsec/PotrfSolveTask.hh"
#include "slate/parsec/tasks.hh"

namespace parsec {
   
template<typename matrix_t, typename scalar_t>
class PotrfPanelTask {
public:

   struct Arguments {
      // Tile row and column index
      int64_t k;
      // Matrix pointer
      matrix_t *a;

      // Default constructor
      Arguments() :
         k(-1), a(nullptr) { }

      Arguments(int64_t in_k, matrix_t *in_a)
         : k(in_k), a(in_a) { }
   };

   void initialize(Arguments const& in_args) {

      this->args_ = Arguments(in_args);
   }

   // Run the task 
   void execute() {
      // TODO: check task has been initialized e.g. a != 0
      this->cpu_func(this->args_.k, this->args_.a);
   }

   // Submit task
   void submit(parsec_taskpool_t *tp, int priority = 0) {

      // TODO: check task has been initialized e.g. a != 0
      
      auto k = args_.k;
      auto a = this->args_.a;

      // Number of row and column tiles
      const int64_t A_nt = a->nt();

      auto *column_k = a->column(k);
      
      // Retrieve rank owning tile a(k, k)
      // int rank = this->args_.a->tileRank(k, k);

      parsec_dtd_taskpool_insert_task(
            tp,
            // parsec_potrf_cpu_func<matrix_t, scalar_t>, // Debug
            parsec_cpu_func,
            priority, /* inherits the priority from high level task */
            "PotrfPanelTask",
            // We create the dependency to wait for the communication
            sizeof(decltype(k)), &k, VALUE,
            sizeof(matrix_t *), &a, VALUE,
            PASSED_BY_REF, column_k, INOUT | AFFINITY,
            PARSEC_DTD_ARG_END);
      
   }
   
private:      

   Arguments args_;

   static int parsec_cpu_func(
         parsec_execution_stream_t *es, parsec_task_t *this_task) {
      
      int64_t k;
      matrix_t *a;
      int32_t *column_k = nullptr;

      parsec_dtd_unpack_args(
            this_task, &k, &a, &column_k);

      cpu_func(k,  a);

      // std::cout << "[PotrfPanelTask::parsec_cpu_func] done" << std::endl;

      return PARSEC_HOOK_RETURN_DONE;
   }
   
   static void cpu_func(int64_t k, matrix_t *a) {

      // Number of row and column tiles
      const int64_t A_nt = a->nt();

      parsec_taskpool_t *tp = parsec_dtd_taskpool_new();
      // parsec_taskpool_t *tp = a->parsec_potrf_panel_tp;
      a->enqueue_taskpool(tp);

      //
      // Potrf A(k,k)

      {
         // Task type
         // using PotrfTask = parsec::PotrfTask<matrix_t, scalar_t>; 
         using PotrfTask = parsec::PotrfDataTask<matrix_t, scalar_t>; 
         // Task arguments 
         typename PotrfTask::Arguments potrf_args(k, a);
         // Initialize task 
         PotrfTask potrf_task;
         potrf_task.initialize(potrf_args);

         potrf_task.submit(tp);
      }
            
      //
      // Bcast A(k,k)

      // auto* akk_dtd_tile = a->desc->dtd_tiles[a->desc->super.data_key(&(a->desc->super), k, k)];

      // Send diagonal tile A(k, k) to sub diagonal tile A(k+1:nt, k)
      // if (k+1 < A_nt) {

      //    using SubDiagBcast = parsec::TileBcastTask<matrix_t, scalar_t>;
      //    typename SubDiagBcast::Arguments bcast_args(k, k, {k+1, A_nt-1, k, k}, a);
      //    SubDiagBcast bcast_task;
      //    bcast_task.initialize(bcast_args);
      //    // bcast_task.execute();
         
      //    bcast_task.submit(tp);

      // }

      // parsec_dtd_data_flush(tp, akk_dtd_tile);
      // parsec_dtd_taskpool_wait(tp);

      //
      // Trsm A(k+1:A_nt,k)

      if (k+1 <= A_nt-1) {

         for (int64_t i = k+1; i < A_nt; ++i) {

            //
            // Trsm A(i,k)

            // Task type
            using TrsmTask = parsec::PotrfSolveDataTask<matrix_t, scalar_t>; 
            // using TrsmTask = parsec::PotrfSolveTask<matrix_t, scalar_t>; 
            // Task arguments
            typename TrsmTask::Arguments trsm_args(i, k, a);

            TrsmTask trsm_task;
            trsm_task.initialize(trsm_args);

            // trsm_task.execute(); // Synchronous execution

            // Asynchronous execution (submitted to high_level taskpool)
            trsm_task.submit(tp);

            // Tile Bcast
            
            using TrailingBcast = parsec::TileBcastTask<matrix_t, scalar_t>;
            // Send A(i,k) to A(i, k+1:i) and to A(i+1:nt, i)
            typename TrailingBcast::Arguments bcast_args(i, k, {{i, i, k+1, i}, {i+1, A_nt-1, i, i}}, a);
            TrailingBcast bcast_task;
            bcast_task.initialize(bcast_args);
            // bcast1_task.execute();

            bcast_task.submit(tp);

            // Flush A(i,k) tile
            // auto* aik_dtd_tile = a->desc->dtd_tiles[a->desc->super.data_key(&(a->desc->super), i, k)];
            // parsec_dtd_data_flush(tp, aik_dtd_tile);

         }

         // parsec_dtd_taskpool_wait(tp);
      }

      // parsec_dtd_data_flush(tp, akk_dtd_tile);

      for (int64_t i = k; i < A_nt; ++i) {
         auto* aik_dtd_tile = a->desc->dtd_tiles[a->desc->super.data_key(&(a->desc->super), i, k)];
         parsec_dtd_data_flush(tp, aik_dtd_tile);
      }
      
      // std::cout << "[PotrfPanelTask::cpu_func] my rank = " << a->mpiRank()
      //           << ", Potrf A(:, " << k <<  ").." << std::endl;

      parsec_dtd_taskpool_wait(tp);

      // std::cout << "[PotrfPanelTask::cpu_func] my rank = " << a->mpiRank()
      //           << ", done" << std::endl;

      // parsec_dtd_dequeue_taskpool(tp);
      parsec_taskpool_free(tp);
   }

};
   
}

#endif POTRF_PANEL_TASK_HH
