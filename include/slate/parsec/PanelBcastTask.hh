#ifndef PARSEC_PANEL_BCAST_TASK_HH
#define PARSEC_PANEL_BCAST_TASK_HH

#include "slate/parsec/TileBcastTask.hh"

namespace parsec {

template<typename matrix_t, typename scalar_t>
class PanelBcastTask {
public:

   struct Arguments {
      // Tile row and column index
      int64_t k;
      // Matrix pointer
      matrix_t *a;

      // Default constructor
      Arguments() :
         k(-1), a(nullptr) { }

      Arguments(int64_t in_k, matrix_t *in_a)
         : k(in_k), a(in_a) { }
   };

   void initialize(Arguments const& in_args) {

      this->args_ = Arguments(in_args);
   }

   // Execute task kernel 
   void execute() {
      // TODO: check task has been initialized e.g. a != 0
      this->cpu_func(this->args_.k, this->args_.a);
   }

   void submit(parsec_taskpool_t *tp, int priority = 0) {

      // TODO: check task has been initialized e.g. a != 0
      
      auto k = args_.k;
      auto a = this->args_.a;

      // Number of row and column tiles
      const int64_t A_nt = a->nt();

      auto *column_k = a->column(k);
      auto *column_k_plus_one = a->column(k+1);
      auto *column_n = a->column(A_nt-1);

      parsec_dtd_taskpool_insert_task(
            tp,
            // parsec_potrf_cpu_func<matrix_t, scalar_t>, // Debug
            parsec_cpu_func,
            priority, /* inherits the priority from high level task */
            "PanelBcastTask",
            // We create the dependency to wait for the communication
            sizeof(decltype(k)), &k, VALUE,
            sizeof(matrix_t *), &a, VALUE,
            PASSED_BY_REF, column_k, INPUT,
            PASSED_BY_REF, column_k_plus_one, INOUT,
            PASSED_BY_REF, column_n, INOUT | AFFINITY,
            PARSEC_DTD_ARG_END);
      
   }   

private:      

   Arguments args_;

   static int parsec_cpu_func(
         parsec_execution_stream_t *es, parsec_task_t *this_task) {
      
      int64_t k;
      matrix_t *a;
      int32_t *column_k = nullptr;
      int32_t *column_k_plus_one = nullptr;
      int32_t *column_n = nullptr;

      parsec_dtd_unpack_args(
            this_task, &k, &a, &column_k, &column_k_plus_one, &column_n);

      cpu_func(k,  a);

      // std::cout << "[PotrfPanelTask::parsec_cpu_func] done" << std::endl;

      return PARSEC_HOOK_RETURN_DONE;
   }

   static void cpu_func(int64_t k, matrix_t *a) {

      // Number of row and column tiles
      const int64_t A_nt = a->nt();

      parsec_taskpool_t *tp = parsec_dtd_taskpool_new();
      a->enqueue_taskpool(tp);

      // std::cout << "[PanelBcastTask::cpu_func]" << std::endl;

      // Send tiles in current block-column to trailing submatrix
      if (k+1 < A_nt) {

         for (int64_t i = k+1; i < A_nt; ++i) {
            
            using TrailingBcast = parsec::TileBcastTask<matrix_t, scalar_t>;
            // Send A(i,k) to A(i, k+1:i) and to A(i+1:nt, i)
            typename TrailingBcast::Arguments bcast1_args(i, k, {{i, i, k+1, i}, {i+1, A_nt-1, i, i}}, a);
            TrailingBcast bcast1_task;
            bcast1_task.initialize(bcast1_args);
            // bcast1_task.execute();

            bcast1_task.submit(tp);
            // parsec_dtd_taskpool_wait(A.parsec_context, tp);

            // Send A(i,k) to A(i+1:nt, i)

            // // typename TrailingBcast::Arguments bcast2_args(i, k, {i, A_nt-1, i, i}, &A);
            // typename TrailingBcast::Arguments bcast2_args(i, k, {i+1, A_nt-1, i, i}, &A);
            // TrailingBcast bcast2_task;
            // bcast2_task.initialize(bcast2_args);
            // // bcast2_task.execute();
            // bcast2_task.submit(tp);
            // // parsec_dtd_taskpool_wait(A.parsec_context, tp);      

            // Flush A(i,k) tile
            auto* aik_dtd_tile = a->desc->dtd_tiles[a->desc->super.data_key(&(a->desc->super), i, k)];
            parsec_dtd_data_flush(tp, aik_dtd_tile);
         }

         parsec_dtd_taskpool_wait(tp);
      }
      
      parsec_dtd_dequeue_taskpool(tp);
      parsec_taskpool_free(tp);
      
   }
   
};   

}

#endif
