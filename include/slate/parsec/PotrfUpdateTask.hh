#ifndef POTRF_UPDATE_TASK_HH
#define POTRF_UPDATE_TASK_HH

#include <iostream>

#include "slate/parsec/tasks.hh"
#include "internal/internal.hh"
#include "internal/internal_batch.hh"

namespace parsec {

#ifdef PARSEC_DTD_HAVE_CUDA


   
template<typename matrix_t, typename scalar_t>
class PotrfUpdateGpuTask { // TODO: create proper Gemm Parsec task
public:
   struct Arguments {
      // Source row/column tile index
      int64_t k;
      // Destination tile row index
      int64_t i;
      // Destination tile column index
      int64_t j;
      // Matrix pointer
      matrix_t *a;
      // Push out data to main memory
      bool pushout;

      // Default constructor
      Arguments() :
         k(-1), i(-1), j(-1), a(nullptr), pushout(false) { }

      Arguments(int64_t in_k, int64_t in_i, int64_t in_j, matrix_t *in_a)
         : k(in_k), i(in_i), j(in_j), a(in_a), pushout(false) { }
   };

   PotrfUpdateGpuTask() { }

   void initialize(Arguments const& in_args) {
      this->args_ = Arguments(in_args);
   }

   // Insert task
   void submit(parsec_taskpool_t *tp, int priority = 0) {

      auto* a = args_.a;
      auto k = args_.k;
      auto i = args_.i;
      auto j = args_.j;

      // Retrieve rank owning tile a(i, j)
      int tile_rank = a->tileRank(i, j);

      // A(i, k) tile
      auto* aik_dtd_tile = a->desc->dtd_tiles[a->desc->super.data_key(&(a->desc->super), i, k)];
      // int aik_arena_index = (a->tileIsLocal(i, k)) ? parsec::LAPACK_TILE : parsec::FULL_TILE;
      int aik_arena_index = parsec::FULL_TILE;
         
      // A(j, k) tile
      auto* ajk_dtd_tile = a->desc->dtd_tiles[a->desc->super.data_key(&(a->desc->super), j, k)];
      // int ajk_arena_index = (a->tileIsLocal(j, k)) ? parsec::LAPACK_TILE : parsec::FULL_TILE;
      int ajk_arena_index = parsec::FULL_TILE;
      
      // A(i, j) tile
      auto* aij_dtd_tile = a->desc->dtd_tiles[a->desc->super.data_key(&(a->desc->super), i, j)];
      // int aij_arena_index = (a->tileIsLocal(i, j)) ? parsec::LAPACK_TILE : parsec::FULL_TILE;
      int aij_arena_index = parsec::FULL_TILE;
      
      int aij_op_type = INOUT | aij_arena_index;

      // If udpate takes places in block column right after the panel,
      // then we send the data back to the CPU after performing the
      // update.

      if (args_.pushout) {
         aij_op_type |= PUSHOUT;
      }

      if (a->tileIsLocal(i, j)) {
         int cuda_dev_index = parsec::get_tile_device_index(a, i, j);

         // std::cout << "[PotrfUpdateGpuTask::gpu_func]  k = " << k
         //           << ", A(" << i << ", " << j <<  ")"
         //           << ", device index = " << parsec::get_tile_device_index(a, i, j)
         //           << std::endl;
         
         parsec_data_copy_t *data_copy = aij_dtd_tile->data_copy;
         assert(NULL != data_copy);
         parsec_data_t *data = data_copy->original;
         assert(NULL != data);
         parsec_advise_data_on_device(
               data, cuda_dev_index, PARSEC_DEV_DATA_ADVICE_PREFERRED_DEVICE);

      }

      parsec_dtd_taskpool_insert_cuda_task(
            tp,
            parsec_gpu_func,
            priority, /* inherits the priority from high level task */
            "PotrfUpdateGpuTask",
            sizeof(decltype(k)), &k, VALUE,
            sizeof(decltype(i)), &i, VALUE,
            sizeof(decltype(j)), &j, VALUE,
            sizeof(matrix_t *), &args_.a, VALUE,
            PASSED_BY_REF, aik_dtd_tile, INPUT | aik_arena_index,
            PASSED_BY_REF, ajk_dtd_tile, INPUT | ajk_arena_index,
            PASSED_BY_REF, aij_dtd_tile, aij_op_type,
            sizeof(int), &tile_rank, VALUE | AFFINITY,
            PARSEC_DTD_ARG_END);
         
   }

private:

   Arguments args_;

   static void gpu_func(
         cublasHandle_t cuhandle, cudaStream_t cuda_stream,
         int64_t k, int64_t i, int64_t j, matrix_t *a, scalar_t *dev_aik_data,
         scalar_t *dev_ajk_data, scalar_t *dev_aij_data) {

      if (a->tileIsLocal(i, j)) {

         int devid;
         cudaError_t err = cudaGetDevice(&devid);

         // std::cout << "[PotrfUpdateGpuTask::gpu_func]  k = " << k
         //           << ", A(" << i << ", " << j <<  ")"
         //           << ", devid = " << devid
         //           << std::endl;

         // Destination tile
         auto aij = a->at(i, j);
         auto aij_stride = aij.stride();

         int mb = aij.mb();
         int nb = aij.nb();

         auto aik_mb = a->tileMb(i);
         auto ajk_mb = a->tileMb(j);

         int kb = a->tileNb(k);

         auto aik_op = slate::Op::NoTrans;
         auto ajk_op = slate::Op::Trans;
         
         auto alpha = scalar_t(-1.0);
         auto beta = scalar_t( 1.0);

         // std::cout << "[PotrfUpdateGpuTask::gpu_func] aik_mb = " << aik_mb
         //           << ", ajk_mb = " << ajk_mb
         //           << std::endl;

         
         // std::cout << "[PotrfUpdateGpuTask::gpu_func] mb = " << mb
         //           << ", nb = " << nb
         //           << ", kb = " << kb
         //           << std::endl;

         slate_cublas_call(
               parsec::cublasGemm(
                     cuhandle,  // uses stream
                     slate::internal::cublas_op_const(aik_op),
                     slate::internal::cublas_op_const(ajk_op),
                     mb, nb, kb,
                     &alpha,
                     (const scalar_t*) dev_aik_data, aik_mb,
                     (const scalar_t*) dev_ajk_data, ajk_mb,
                     &beta, dev_aij_data, mb));

               // slate::Tile<scalar_t> aik(
               //       aik_mb, nb, aik_data, aik_stride, slate::HostNum,
               //       slate::TileKind::UserOwned, slate::Layout::ColMajor);

               // slate::Tile<scalar_t> ajk(
               //       ajk_mb, nb, ajk_data, ajk_stride, slate::HostNum,
               //       slate::TileKind::UserOwned, slate::Layout::ColMajor);

               // auto trans_ajk = slate::conj_transpose(ajk);

               // slate::gemm<scalar_t>(scalar_t(-1.0), aik, trans_ajk, scalar_t(1.0), aij);

         // slate_cuda_call(
         //       cudaStreamSynchronize(cuda_stream));
         
         // slate_cuda_call(
         //       cudaDeviceSynchronize());
            
      }
   }
   
   static int parsec_gpu_func(
         void *gpu_device,
         cudaStream_t cuda_stream, parsec_task_t *this_task) {

      int64_t k;
      int64_t i;
      int64_t j;
      matrix_t *a;
      scalar_t *aik_data;
      scalar_t *ajk_data;
      scalar_t *aij_data;
      int tile_rank;
         
      parsec_dtd_unpack_args(
            this_task, &k, &i, &j, &a, &aik_data, &ajk_data, &aij_data,
            &tile_rank);

      // std::cout << "[PotrfUpdateGpuTask::parsec_gpu_func]  A(" << i << ", " << j <<  ")" << std::endl;

      scalar_t *dev_aik_data = (scalar_t *)parsec_dtd_get_dev_ptr(this_task, 0);
      scalar_t *dev_ajk_data = (scalar_t *)parsec_dtd_get_dev_ptr(this_task, 1);
      scalar_t *dev_aij_data = (scalar_t *)parsec_dtd_get_dev_ptr(this_task, 2);

      // std::cout << "[PotrfUpdateGpuTask::parsec_gpu_func] dev_aik_data = " << dev_aik_data
      //           << ", dev_ajk_data = " << dev_ajk_data
      //           << ", dev_aij_data = " << dev_aij_data
      //           << std::endl;

      // // DEBUG
      // scalar_t x, y, z;
      // cudaMemcpyAsync(
      //    &x, dev_aik_data, sizeof(scalar_t),
      //    cudaMemcpyDeviceToHost, cuda_stream);   
      // cudaMemcpyAsync(
      //    &y, dev_ajk_data, sizeof(scalar_t),
      //    cudaMemcpyDeviceToHost, cuda_stream);   
      // cudaMemcpyAsync(
      //    &z, dev_aij_data, sizeof(scalar_t),
      //    cudaMemcpyDeviceToHost, cuda_stream);
      // cudaStreamSynchronize(cuda_stream);
      
      // // std::cout << "[PotrfUpdateGpuTask::parsec_gpu_func]"
      // std::cout << "[PotrfUpdateGpuTask::parsec_gpu_func]"
      //           << " aik = " << dev_aik_data << ", ajk = " << dev_ajk_data << ", aij = " << dev_aij_data
      //           << " aik = " << x << ", ajk = " << y << ", aij = " << z
      //           << std::endl;

      // printf("[PotrfUpdateGpuTask::parsec_gpu_func] transfer status = %d\n", this_task->data[0].data_out->data_transfer_status);
      // assert(NULL);
      cublasStatus_t status;
      cublasHandle_t cuhandle = NULL;

      // status = cublasCreate(&cuhandle);
      // status = cublasSetStream(cuhandle, cuda_stream);

      cuhandle = parsec::get_cublas_handle(cuda_stream);

      assert(NULL != cuhandle);

      gpu_func(
            cuhandle, cuda_stream,
            k, i, j, a, dev_aik_data, dev_ajk_data, dev_aij_data);
      
      return PARSEC_HOOK_RETURN_DONE;
   }

};

template<typename matrix_t, typename scalar_t>
class PotrfUpdateTCTask { // TODO: create proper Gemm Parsec task
public:
   struct Arguments {
      // Source row/column tile index
      int64_t k;
      // Destination tile row index
      int64_t i;
      // Destination tile column index
      int64_t j;
      // Matrix pointer
      matrix_t *a;
      // Push out data to main memory
      bool pushout;

      // Default constructor
      Arguments() :
         k(-1), i(-1), j(-1), a(nullptr), pushout(false) { }

      Arguments(int64_t in_k, int64_t in_i, int64_t in_j, matrix_t *in_a)
         : k(in_k), i(in_i), j(in_j), a(in_a), pushout(false) { }
   };

   PotrfUpdateTCTask() { }

   void initialize(Arguments const& in_args) {
      this->args_ = Arguments(in_args);
   }

   // Insert task
   void submit(parsec_taskpool_t *tp, int priority = 0) {

      auto* a = args_.a;
      auto k = args_.k;
      auto i = args_.i;
      auto j = args_.j;

      // Retrieve rank owning tile a(i, j)
      int tile_rank = a->tileRank(i, j);

      // A(i, k) tile
      auto* aik_dtd_tile = a->desc->dtd_tiles[a->desc->super.data_key(&(a->desc->super), i, k)];
      // int aik_arena_index = (a->tileIsLocal(i, k)) ? parsec::LAPACK_TILE : parsec::FULL_TILE;
      int aik_arena_index = parsec::FULL_TILE;
         
      // A(j, k) tile
      auto* ajk_dtd_tile = a->desc->dtd_tiles[a->desc->super.data_key(&(a->desc->super), j, k)];
      // int ajk_arena_index = (a->tileIsLocal(j, k)) ? parsec::LAPACK_TILE : parsec::FULL_TILE;
      int ajk_arena_index = parsec::FULL_TILE;
      
      // A(i, j) tile
      auto* aij_dtd_tile = a->desc->dtd_tiles[a->desc->super.data_key(&(a->desc->super), i, j)];
      // int aij_arena_index = (a->tileIsLocal(i, j)) ? parsec::LAPACK_TILE : parsec::FULL_TILE;
      int aij_arena_index = parsec::FULL_TILE;
      
      int aij_op_type = INOUT | aij_arena_index;

      // If udpate takes places in block column right after the panel,
      // then we send the data back to the CPU after performing the
      // update.

      if (args_.pushout) {
         aij_op_type |= PUSHOUT;
      }

      if (a->tileIsLocal(i, j)) {
         int cuda_dev_index = parsec::get_tile_device_index(a, i, j);

         // std::cout << "[PotrfUpdateGpuTask::gpu_func]  k = " << k
         //           << ", A(" << i << ", " << j <<  ")"
         //           << ", device index = " << parsec::get_tile_device_index(a, i, j)
         //           << std::endl;
         
         parsec_data_copy_t *data_copy = aij_dtd_tile->data_copy;
         assert(NULL != data_copy);
         parsec_data_t *data = data_copy->original;
         assert(NULL != data);
         parsec_advise_data_on_device(
               data, cuda_dev_index, PARSEC_DEV_DATA_ADVICE_PREFERRED_DEVICE);

      }

      parsec_dtd_taskpool_insert_cuda_task(
            tp,
            parsec_gpu_func,
            priority, /* inherits the priority from high level task */
            "PotrfUpdateGpuTask",
            sizeof(decltype(k)), &k, VALUE,
            sizeof(decltype(i)), &i, VALUE,
            sizeof(decltype(j)), &j, VALUE,
            sizeof(matrix_t *), &args_.a, VALUE,
            PASSED_BY_REF, aik_dtd_tile, INPUT | aik_arena_index,
            PASSED_BY_REF, ajk_dtd_tile, INPUT | ajk_arena_index,
            PASSED_BY_REF, aij_dtd_tile, aij_op_type,
            sizeof(int), &tile_rank, VALUE | AFFINITY,
            PARSEC_DTD_ARG_END);
         
   }

private:

   Arguments args_;

   static void gpu_func(
         cublasHandle_t cuhandle, cudaStream_t cuda_stream,
         int64_t k, int64_t i, int64_t j, matrix_t *a, scalar_t *dev_aik_data,
         scalar_t *dev_ajk_data, scalar_t *dev_aij_data) {

      if (a->tileIsLocal(i, j)) {

         int devid;
         cudaError_t err = cudaGetDevice(&devid);

         // std::cout << "[PotrfUpdateGpuTask::gpu_func]  k = " << k
         //           << ", A(" << i << ", " << j <<  ")"
         //           << ", devid = " << devid
         //           << std::endl;

         // Destination tile
         auto aij = a->at(i, j);
         auto aij_stride = aij.stride();

         int mb = aij.mb();
         int nb = aij.nb();

         auto aik_mb = a->tileMb(i);
         auto ajk_mb = a->tileMb(j);

         int kb = a->tileNb(k);

         auto aik_op = slate::Op::NoTrans;
         auto ajk_op = slate::Op::Trans;
         
         auto alpha = scalar_t(-1.0);
         auto beta = scalar_t( 1.0);

         // std::cout << "[PotrfUpdateGpuTask::gpu_func] aik_mb = " << aik_mb
         //           << ", ajk_mb = " << ajk_mb
         //           << std::endl;

         
         // std::cout << "[PotrfUpdateGpuTask::gpu_func] mb = " << mb
         //           << ", nb = " << nb
         //           << ", kb = " << kb
         //           << std::endl;

         // cublasSetMathMode(cuhandle, CUBLAS_DEFAULT_MATH);
         
         slate_cublas_call(
               parsec::cublasGemmEx(
                     cuhandle,  // uses stream
                     slate::internal::cublas_op_const(aik_op),
                     slate::internal::cublas_op_const(ajk_op),
                     mb, nb, kb,
                     &alpha,
                     (const scalar_t*) dev_aik_data, aik_mb,
                     (const scalar_t*) dev_ajk_data, ajk_mb,
                     &beta, dev_aij_data, mb));

         // slate::Tile<scalar_t> aik(
         //       aik_mb, nb, aik_data, aik_stride, slate::HostNum,
         //       slate::TileKind::UserOwned, slate::Layout::ColMajor);

         // slate::Tile<scalar_t> ajk(
         //       ajk_mb, nb, ajk_data, ajk_stride, slate::HostNum,
         //       slate::TileKind::UserOwned, slate::Layout::ColMajor);

         // auto trans_ajk = slate::conj_transpose(ajk);

         // slate::gemm<scalar_t>(scalar_t(-1.0), aik, trans_ajk, scalar_t(1.0), aij);

         // slate_cuda_call(
         //       cudaStreamSynchronize(cuda_stream));
         
         // slate_cuda_call(
         //       cudaDeviceSynchronize());
            
      }
   }
   
   static int parsec_gpu_func(
         void *gpu_device,
         cudaStream_t cuda_stream, parsec_task_t *this_task) {

      int64_t k;
      int64_t i;
      int64_t j;
      matrix_t *a;
      scalar_t *aik_data;
      scalar_t *ajk_data;
      scalar_t *aij_data;
      int tile_rank;
         
      parsec_dtd_unpack_args(
            this_task, &k, &i, &j, &a, &aik_data, &ajk_data, &aij_data,
            &tile_rank);

      // std::cout << "[PotrfUpdateGpuTask::parsec_gpu_func]  A(" << i << ", " << j <<  ")" << std::endl;

      scalar_t *dev_aik_data = (scalar_t *)parsec_dtd_get_dev_ptr(this_task, 0);
      scalar_t *dev_ajk_data = (scalar_t *)parsec_dtd_get_dev_ptr(this_task, 1);
      scalar_t *dev_aij_data = (scalar_t *)parsec_dtd_get_dev_ptr(this_task, 2);

      // std::cout << "[PotrfUpdateGpuTask::parsec_gpu_func] dev_aik_data = " << dev_aik_data
      //           << ", dev_ajk_data = " << dev_ajk_data
      //           << ", dev_aij_data = " << dev_aij_data
      //           << std::endl;

      // // DEBUG
      // scalar_t x, y, z;
      // cudaMemcpyAsync(
      //    &x, dev_aik_data, sizeof(scalar_t),
      //    cudaMemcpyDeviceToHost, cuda_stream);   
      // cudaMemcpyAsync(
      //    &y, dev_ajk_data, sizeof(scalar_t),
      //    cudaMemcpyDeviceToHost, cuda_stream);   
      // cudaMemcpyAsync(
      //    &z, dev_aij_data, sizeof(scalar_t),
      //    cudaMemcpyDeviceToHost, cuda_stream);
      // cudaStreamSynchronize(cuda_stream);
      
      // // std::cout << "[PotrfUpdateGpuTask::parsec_gpu_func]"
      // std::cout << "[PotrfUpdateGpuTask::parsec_gpu_func]"
      //           << " aik = " << dev_aik_data << ", ajk = " << dev_ajk_data << ", aij = " << dev_aij_data
      //           << " aik = " << x << ", ajk = " << y << ", aij = " << z
      //           << std::endl;

      // printf("[PotrfUpdateGpuTask::parsec_gpu_func] transfer status = %d\n", this_task->data[0].data_out->data_transfer_status);
      // assert(NULL);
      cublasStatus_t status;
      cublasHandle_t cuhandle = NULL;

      // status = cublasCreate(&cuhandle);
      // status = cublasSetStream(cuhandle, cuda_stream);

      cuhandle = parsec::get_cublas_handle(cuda_stream);

      assert(NULL != cuhandle);

      gpu_func(
            cuhandle, cuda_stream,
            k, i, j, a, dev_aik_data, dev_ajk_data, dev_aij_data);
      
      return PARSEC_HOOK_RETURN_DONE;
   }

};
   
#endif
   
   //
   // Potrf update Parsec task with data dependency tracking
   //

template<typename matrix_t, typename scalar_t>
class PotrfUpdateDataTask { // TODO: create proper Gemm Parsec task
public:
   struct Arguments {
      // Source row/column tile index
      int64_t k;
      // Destination tile row index
      int64_t i;
      // Destination tile column index
      int64_t j;
      // Matrix pointer
      matrix_t *a;

      // Default constructor
      Arguments() :
         k(-1), i(-1), j(-1), a(nullptr) { }

      Arguments(int64_t in_k, int64_t in_i, int64_t in_j, matrix_t *in_a)
         : k(in_k), i(in_i), j(in_j), a(in_a) { }
   };

   PotrfUpdateDataTask() { }

   // Run the task 
   // void execute() {
   //    // TODO: check task has been initialized e.g. a != 0
   //    this->cpu_func(this->args_.k, this->args_.i, this->args_.j, this->args_.a);
   // }

   void initialize(Arguments const& in_args) {
      this->args_ = Arguments(in_args);
   }

   // Insert task
   void submit(parsec_taskpool_t *tp, int priority = 0) {

      auto* a = args_.a;
      auto k = args_.k;
      auto i = args_.i;
      auto j = args_.j;

      // Retrieve rank owning tile a(i, j)
      int tile_rank = a->tileRank(i, j);

      // A(i, k) tile
      auto* aik_dtd_tile = a->desc->dtd_tiles[a->desc->super.data_key(&(a->desc->super), i, k)];
      int aik_arena_index = (a->tileRank(i, k) == a->mpiRank()) ? parsec::LAPACK_TILE : parsec::FULL_TILE;

      // A(j, k) tile
      auto* ajk_dtd_tile = a->desc->dtd_tiles[a->desc->super.data_key(&(a->desc->super), j, k)];
      int ajk_arena_index = (a->tileRank(j, k) == a->mpiRank()) ? parsec::LAPACK_TILE : parsec::FULL_TILE;

      // A(i, j) tile
      auto* aij_dtd_tile = a->desc->dtd_tiles[a->desc->super.data_key(&(a->desc->super), i, j)];
      int aij_arena_index = (a->tileRank(i, j) == a->mpiRank()) ? parsec::LAPACK_TILE : parsec::FULL_TILE;

      parsec_dtd_taskpool_insert_task(
            tp,
            parsec_cpu_func,
            priority, /* inherits the priority from high level task */
            "PotrfUpdateTask",
            sizeof(decltype(k)), &k, VALUE,
            sizeof(decltype(j)), &i, VALUE,
            sizeof(decltype(j)), &j, VALUE,
            sizeof(matrix_t *), &args_.a, VALUE,
            PASSED_BY_REF, aik_dtd_tile, INPUT | aik_arena_index /*| AFFINITY*/,
            PASSED_BY_REF, ajk_dtd_tile, INPUT | ajk_arena_index /*| AFFINITY*/,
            PASSED_BY_REF, aij_dtd_tile, INOUT | aij_arena_index /*| AFFINITY*/,
            sizeof(int), &tile_rank, VALUE | AFFINITY,
            PARSEC_DTD_ARG_END);
         
   }

   
private:

   Arguments args_;

   static void cpu_func(
         int64_t k, int64_t i, int64_t j, matrix_t *a, scalar_t *aik_data,
         scalar_t *ajk_data, scalar_t *aij_data) {

      if (a->tileIsLocal(i, j)) {

         // std::cout << "[PotrfUpdateDataTask::cpu_func]  A(" << i << ", " << j <<  ")" << std::endl;

         // Destination tile
         auto aij = a->at(i, j);
         auto aij_stride = aij.stride();

         //
         // Source tile A(i,k)
         // auto aik = a->at(i, k);

         auto aik_mb = a->tileMb(i);
         auto nb = a->tileNb(k);

         auto aik_stride = (a->tileIsLocal(i, k)) ? a->at(i, k).stride() : a->ld_arena;

         slate::Tile<scalar_t> aik(
               aik_mb, nb, aik_data, aik_stride, slate::HostNum,
               slate::TileKind::UserOwned, slate::Layout::ColMajor);

         //
         // Source tile A(j,k)
         // auto ajk = a->at(j, k);

         auto ajk_mb = a->tileMb(j);

         auto ajk_stride = (a->tileIsLocal(j, k)) ? a->at(j, k).stride() : a->ld_arena;

         slate::Tile<scalar_t> ajk(
               ajk_mb, nb, ajk_data, ajk_stride, slate::HostNum,
               slate::TileKind::UserOwned, slate::Layout::ColMajor);

         auto trans_ajk = slate::conj_transpose(ajk);

         slate::gemm<scalar_t>(scalar_t(-1.0), aik, trans_ajk, scalar_t(1.0), aij);

         // Decrease source tiles life counter  
         // A->tileTick(i, k);
         // A->tileTick(j, k);
      }
   }

public:
   
   static int parsec_cpu_func(
         parsec_execution_stream_t *es, parsec_task_t *this_task) {

      (void)es;
         
      int64_t k;
      int64_t i;
      int64_t j;
      matrix_t *a;
      scalar_t *aik_data;
      scalar_t *ajk_data;
      scalar_t *aij_data;
      int tile_rank;
         
      parsec_dtd_unpack_args(
            this_task, &k, &i, &j, &a, &aik_data, &ajk_data, &aij_data,
            &tile_rank);
         
      cpu_func(k, i, j, a, aik_data, ajk_data, aij_data);
         
      return PARSEC_HOOK_RETURN_DONE;      
   }

};
   
   //
   // Potrf update Parsec task
   //

template<typename matrix_t, typename scalar_t>
class PotrfUpdateTask { // TODO: create proper Gemm Parsec task
public:
   struct Arguments {
      // Source row/column tile index
      int64_t k;
      // Destination tile row index
      int64_t i;
      // Destination tile column index
      int64_t j;
      // Matrix pointer
      matrix_t *a;

      // Default constructor
      Arguments() :
         k(-1), i(-1), j(-1), a(nullptr) { }

      Arguments(int64_t in_k, int64_t in_i, int64_t in_j, matrix_t *in_a)
         : k(in_k), i(in_i), j(in_j), a(in_a) { }
   };

   PotrfUpdateTask() { }
      
   // Run the task 
   void execute() {
      // TODO: check task has been initialized e.g. a != 0
      this->cpu_func(this->args_.k, this->args_.i, this->args_.j, this->args_.a);
   }

   void initialize(Arguments const& in_args) {
      this->args_ = Arguments(in_args);
   }

   // Insert task
   void submit(parsec_taskpool_t *tp, int priority = 0) {

      auto k = args_.k;
      auto i = args_.i;
      auto j = args_.j;
      // Retrieve rank owning tile a(i, j)
      int tile_rank = this->args_.a->tileRank(i, j);

      parsec_dtd_taskpool_insert_task(
            tp,
            parsec_cpu_func,
            priority, /* inherits the priority from high level task */
            "PotrfUpdateTask",
            sizeof(decltype(k)), &k, VALUE,
            sizeof(decltype(j)), &i, VALUE,
            sizeof(decltype(j)), &j, VALUE,
            sizeof(matrix_t *), &args_.a, VALUE,
            sizeof(int), &tile_rank, VALUE | AFFINITY,
            PARSEC_DTD_ARG_END);
         
   }

private:

   Arguments args_;

   static void cpu_func(int64_t k, int64_t i, int64_t j, matrix_t *a) {

      if (a->tileIsLocal(i, j)) {

         // Make sure source tiles have been created locally
         assert(a->tileExists(i, k));
         assert(a->tileExists(j, k));

         // Source tile A(i,k)
         auto aik = a->at(i, k);
         // Source tile A(j,k)
         auto ajk = a->at(j, k);
         auto trans_ajk = slate::conj_transpose(ajk);
         // Destination tile
         auto aij = a->at(i, j);

         slate::gemm<scalar_t>(scalar_t(-1.0), aik, trans_ajk, scalar_t(1.0), aij);

         // Decrease source tiles life counter  
         // A->tileTick(i, k);
         // A->tileTick(j, k);
      }
   }

   static int parsec_cpu_func(
         parsec_execution_stream_t *es, parsec_task_t *this_task) {

      (void)es;
         
      int64_t k;
      int64_t i;
      int64_t j;
      matrix_t *a;
      int tile_rank;
         
      parsec_dtd_unpack_args(this_task, &k, &i, &j, &a, &tile_rank);
         
      cpu_func(k, i, j, a);
         
      return PARSEC_HOOK_RETURN_DONE;      
   }

};
   
} // End of namespace parsec
   
#endif POTRF_UPDATE_TASK_HH
