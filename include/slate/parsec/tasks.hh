#ifndef PARSEC_TASKS_HH
#define PARSEC_TASKS_HH

#include "slate/parsec/matrix_wrapper.hh"

#include <iostream>

#include "internal/Tile_lapack.hh"
#include "slate/TriangularMatrix.hh"
#include "slate/parsec/parsec.hh"

namespace parsec {


// Pull data from the GPU to the main memory   
template<typename matrix_t, typename scalar_t>
class PullinDataTask {
public:
   struct Arguments {
      // Tile row index
      int64_t i;
      // Tile column index
      int64_t j;
      // Matrix pointer
      matrix_t *a;
      
      // Default constructor
      Arguments() :
         i(-1), j(-1), a(nullptr) { }

      Arguments(int64_t in_i, int64_t in_j, matrix_t *in_a)
         : i(in_i), j(in_j), a(in_a) { }
   };

   void initialize(Arguments const& in_args) {
      this->args_ = Arguments(in_args);
   }

   // Insert task
   void submit(parsec_taskpool_t *tp, int priority = 0) {

      auto *a = args_.a;
      auto i = args_.i;
      auto j = args_.j;
      // Retrieve rank owning tile a(i, k)
      int tile_rank = this->args_.a->tileRank(i, j);

      // A(i,j) tile
      //
      auto* aij_dtd_tile = a->desc->dtd_tiles[a->desc->super.data_key(&(a->desc->super), i, j)];
      int aij_arena_index = -1;
      // int aik_arena_index = (a->tileIsLocal(i, k)) ? parsec::LAPACK_TILE : parsec::FULL_TILE;
      aij_arena_index = parsec::get_arena_index(a, i, j);

      int aij_op_type = /*INOUT*/INPUT | aij_arena_index | PULLIN;
      
      parsec_dtd_taskpool_insert_task(
            tp,
            parsec_cpu_func,
            priority, /* inherits the priority from high level task */
            "PotrfSolveTask",
            sizeof(decltype(i)), &i, VALUE,
            sizeof(decltype(j)), &j, VALUE,
            sizeof(matrix_t *), &args_.a, VALUE,
            PASSED_BY_REF, aij_dtd_tile, aij_op_type,
            sizeof(int), &tile_rank, VALUE | AFFINITY,
            PARSEC_DTD_ARG_END);

   }

private:

   Arguments args_;

   static void cpu_func() {
      // No-op
   }

   static int parsec_cpu_func(
         parsec_execution_stream_t *es, parsec_task_t *this_task) {

      int64_t i;
      int64_t j;
      matrix_t *a;
      scalar_t *aij_data;
      int tile_rank;

      parsec_dtd_unpack_args(this_task, &i, &j, &a, &aij_data, &tile_rank);

      return PARSEC_HOOK_RETURN_DONE;
   }
};   
   
   // Dummy kernel for debugging purpose 
   int dummy_kernel(parsec_execution_stream_t *es, parsec_task_t *this_task);

   // Dummy task used for debuging purpose. Execute the dummy kernel. 
   void dummy_task(parsec_taskpool_t *tp);
   
   template<typename scalar_t>
   int potrf(parsec_execution_stream_t *es, parsec_task_t *this_task) {
      (void)es;

      void *Akkptr;
      int64_t k;

      parsec_dtd_unpack_args(this_task, &Akkptr, &k);
      (void) k;
      // std::cout << "[potrf] potrf k = " << k << " .."  << std::endl;
      auto Akk = static_cast< slate::Tile<scalar_t>* >(Akkptr);
      // std::cout << "POTRF("<<k<<")" << std::endl;
      slate::potrf<scalar_t>(*Akk);
      delete Akk;
      // std::cout << "[potrf] potrf done" << std::endl;

      // return PARSEC_HOOK_RETURN_ASYNC;
      return PARSEC_HOOK_RETURN_DONE;
   }
   
   template<typename matrix_t, typename scalar_t>
   int trsm(parsec_execution_stream_t *es, parsec_task_t *this_task) {
      (void)es;

      void *Aptr, *Amkptr, *Tkkptr;
      int64_t k, m;
      matrix_t *A;
      (void) k, (void) m;
      
      parsec_dtd_unpack_args(this_task, &Aptr, &Amkptr, &Tkkptr, &k, &m);
      // std::cout << "[trsm] trsm k = " << k << ", m = " << m << " .." << std::endl;

      A = static_cast< matrix_t* >(Aptr);
      auto Tkk = static_cast< slate::Tile<scalar_t>* >(Tkkptr);
      auto Amk = static_cast< slate::Tile<scalar_t>* >(Amkptr);
      //std::cout << "K: "<<k<<" TRSM("<<m<<";"<<k<<")" << std::endl;
      slate::trsm<scalar_t>(blas::Side::Right, blas::Diag::NonUnit, scalar_t(1.0), *Tkk, *Amk);
      // std::cout << "[trsm] trsm done" << std::endl;
      A->tileTick(k, k);

      delete Tkk;
      delete Amk;
      return PARSEC_HOOK_RETURN_DONE;
   }
      
   //
   // Tile send Parsec task
   //

   template<typename matrix_t, typename scalar_t>
   class TileSendTask { // TODO: create proper Gemm Parsec task
   public:

      struct Arguments {
         // source tile row index
         int64_t i;
         // source tile column index
         int64_t j;
         // Destination MPI rank
         int dst_rank;
         // Matrix pointer
         matrix_t *a;

         // Default constructor
         Arguments() :
            i(-1), j(-1), dst_rank(-1), a(nullptr) { }

         Arguments(int64_t in_i, int64_t in_j, int64_t in_dst_rank, matrix_t *in_a)
            : i(in_i), j(in_j), dst_rank(in_dst_rank), a(in_a) { }
      };

      void initialize(Arguments const& in_args) {
         this->args_ = Arguments(in_args);
      }

      // Insert task
      void submit(parsec_taskpool_t *tp, int priority = 0) {

         auto i = this->args_.i;
         auto j = this->args_.j;
         auto dst_rank = this->args_.dst_rank;
         matrix_t *a = this->args_.a;

         int64_t life = 0;
         // TODO: calculate life, for now set it to one
         life = 1; 
         if (!a->tileExists(i, j)) {
            auto* tile = a->tileInsertWorkspace(i, j);
            // std::cout << "[tile_send] my rank = " << A.mpiRank()
            // << ", insert (" << i << "," << k << ") tile " << std::endl;
            a->tileLife(i, j, life);
         }
         
         auto* aij_dtd_tile = a->desc->dtd_tiles[a->desc->super.data_key(&(a->desc->super), i, j)];
         int aij_arena_index = (a->tileRank(i, j) == a->mpiRank()) ? parsec::LAPACK_TILE : parsec::FULL_TILE;

         parsec_dtd_taskpool_insert_task(
               tp,
               parsec_cpu_func, priority,
               "TileSend",
               sizeof(decltype(i)), &i, VALUE,
               sizeof(decltype(j)), &j, VALUE,
               PASSED_BY_REF, aij_dtd_tile, INOUT | aij_arena_index /*| AFFINITY*/,
               // PASSED_BY_REF, aij_dtd_tile, INPUT | aij_arena_index /*| AFFINITY*/,
               sizeof(matrix_t *), &a, VALUE,
               sizeof(int), &dst_rank, VALUE | AFFINITY,
               PARSEC_DTD_ARG_END
               );      

         // parsec_dtd_data_flush(tp, aij_dtd_tile);
         
      }
      
   private:

      Arguments args_;

      static void cpu_func(int64_t i, int64_t j, scalar_t *aij_data, matrix_t *a) {

         
         // std::cout << "[TileSendTask::cpu_func] my rank = " << a->mpiRank()
         //           << ", copy A(" << i << ", " << j << ")"
         //           << ", from rank = " << a->tileRank(i, j)
         //           << std::endl;                  

         // Local tile
         auto aij = a->at(i, j);

         // Copy data receive from A(i,j) owner to local tile
         // memcpy(aij.data(), aij_data, aij.bytes());

         lapack::lacpy(lapack::MatrixType::General,
                       aij.mb(), aij.nb(),
                       aij_data, a->ld_arena,                       
                       aij.data(), aij.stride());

         
      }
      
      static int parsec_cpu_func(
            parsec_execution_stream_t *es, parsec_task_t *this_task) {
         
         int64_t i;
         int64_t j;
         scalar_t *aij_data;
         matrix_t *a;
         int dst_rank;
         
         parsec_dtd_unpack_args(this_task, &i, &j, &aij_data, &a, &dst_rank);

         cpu_func(i, j, aij_data, a);

         return PARSEC_HOOK_RETURN_DONE;
      }

   };
   
   //
   // Tile bcast Parsec task
   //
      
   //
   // Potrf panel kernel
   //

   template<typename matrix_t, typename scalar_t>
   int potrf_panel(
         parsec_execution_stream_t *es,
         parsec_task_t *this_task) {
      (void)es;

      int rc;
      
      std::cout << "[potrf_panel]" << std::endl;

      void *Aptr = nullptr;
      matrix_t *A = nullptr;
      int64_t k = -1;
      int32_t *column_k = nullptr;

      parsec_dtd_unpack_args(this_task, &column_k, &k, &Aptr);

      A = static_cast< matrix_t* >(Aptr);

      // std::cout << "[potrf_panel] A->parsec_potrf_panel_tp = " << A->parsec_potrf_panel_tp << std::endl;

      A->parsec_potrf_panel_tp = parsec_dtd_taskpool_new();
      A->enqueue_taskpool(A->parsec_potrf_panel_tp);

      if (A->tileIsLocal(k, k)) {

         slate::Tile<scalar_t> *Akk = new slate::Tile<scalar_t>(A->at(k,k));

         parsec_dtd_taskpool_insert_task(
               A->parsec_potrf_panel_tp,
               parsec::potrf<scalar_t>,
               this_task->priority, /* inherits the priority from high level task */
               "potrf_potrf",
               // We create the dependency to wait for the communication
               sizeof(slate::Tile<scalar_t>*), Akk, SCRATCH,
               sizeof(int64_t), &k, VALUE,
               PARSEC_DTD_ARG_END);

      }
      
      rc = parsec_dtd_taskpool_wait(A->parsec_context, A->parsec_potrf_panel_tp);
      PARSEC_CHECK_ERROR(rc, "parsec_dtd_taskpool_wait");      

      // std::cout << "[potrf_panel] potrf done" << std::endl;

      // Last block column of matrix. We are done as there are no trsm
      // to be executed.
      if (k == A->nt()-1) 
         return PARSEC_HOOK_RETURN_DONE;

      // Communication somehow Akk -> Amk
      //
      // Send function will create the taskpool for communication and
      // block on it

      // A->parsec_tileSend(k, k, {k+1, A->nt()-1, k, k}, this_task->priority);

      // If we reach this point, this task does not complete. We
      // register its completion callback on the taskpool completion
      parsec_taskpool_set_complete_callback(
            A->parsec_potrf_panel_tp,
            parsec::slate_parsec_complete_tp_callback,
            (void *)this_task);

      for (int64_t m = k+1; m < A->nt(); ++m) {
         // int64_t m = k+1;

         if (A->tileIsLocal(m, k)) {

            // std::cout << "[potrf_panel] insert trsm, m = " << m << ", k = " << k << std::endl;

            auto akk = A->sub(k,k);
            auto tkk = slate::TriangularMatrix<scalar_t>(
                  A->uplo(), slate::Diag::NonUnit, akk);
            auto conjTkk = slate::conj_transpose(tkk);

            slate::Tile<scalar_t> *Amk = new slate::Tile<scalar_t>(A->at( m,k));
            slate::Tile<scalar_t> *Tkk = new slate::Tile<scalar_t>(conjTkk.at(0,0));

            parsec_dtd_taskpool_insert_task(  // Inserting panel macro task
                  A->parsec_potrf_panel_tp,
                  parsec::trsm<matrix_t, scalar_t>, this_task->priority,
                  "potrf_trsm",
                  sizeof(void*),   Aptr, SCRATCH,
                  sizeof(void*),   Amk,  SCRATCH,
                  sizeof(void*),   Tkk,  SCRATCH,
                  sizeof(int64_t), &k,   VALUE,
                  sizeof(int64_t), &m,   VALUE,
                  PARSEC_DTD_ARG_END);
         }
      }

      // rc = parsec_dtd_taskpool_wait(A->parsec_context, A->parsec_potrf_panel_tp);
      // PARSEC_CHECK_ERROR(rc, "parsec_dtd_taskpool_wait");
      // return PARSEC_HOOK_RETURN_DONE;

      parsec_dtd_dequeue_taskpool(
            A->parsec_potrf_panel_tp,
            A->parsec_potrf_panel_tp->context);

      // parsec_taskpool_free(A->parsec_potrf_panel_tp);

      return PARSEC_HOOK_RETURN_ASYNC;
   }

   template<typename matrix_t, typename scalar_t>
   int Amk_comm(
         parsec_execution_stream_t *es,
         parsec_task_t *this_task) {
      (void)es;

      void *Aptr;
      matrix_t *A;
      int64_t k;
      int32_t *column_k, *column_kp1, *column_n;
      // return PARSEC_HOOK_RETURN_DONE;

      parsec_dtd_unpack_args(this_task, &column_k, &column_kp1, &column_n, &k, &Aptr);

      A = static_cast< matrix_t* >(Aptr);

      parsec_taskpool_t *comm_tp = parsec_dtd_taskpool_new();

      A->enqueue_taskpool(comm_tp);

      // Loop over block-rows
      for (int64_t m = k+1; m < A->nt(); ++m) {

         std::cout << "[Amk_comm] send k = " << k << " to m = " << m << std::endl;
         std::cout << "[Amk_comm] ranl = " << A->mpiRank() << std::endl;

         // Push a lot of comms before blocking
         A->parsec_async_tileSend(
               comm_tp,
               m, k, // Source tile indexes
               // Destination tiles: from (m, k+1) to (m, m)
               {m, m, k+1, m},
               // Destination tiles: from (m+1, m) to (nt-1, m) 
               {m+1, A->nt()-1, m, m},
               this_task->priority);
      }

      parsec_dtd_taskpool_wait(comm_tp);
      parsec_dtd_dequeue_taskpool(comm_tp);
      parsec_taskpool_free(comm_tp);

      return PARSEC_HOOK_RETURN_DONE;
   }
   
   template<typename matrix_t, typename scalar_t>
   int herk(parsec_execution_stream_t *es, parsec_task_t *this_task) {
      (void)es;

      void *Aptr, *Ankptr, *Annptr;
      int64_t k, n;
      matrix_t *A;

      (void) k; (void) n;
      parsec_dtd_unpack_args(this_task, &Aptr, &Annptr, &Ankptr, &k, &n);

      A = static_cast< matrix_t* >(Aptr);
      auto Ank = static_cast< slate::Tile<scalar_t>* >(Ankptr);
      auto Ann = static_cast< slate::Tile<scalar_t>* >(Annptr);
      // std::cout << "K: "<<k<<" HERK("<<n<<";"<<n<<")" << std::endl;
      slate::herk<scalar_t>(
            blas::real_type<scalar_t>(-1.0), *Ank,
            blas::real_type<scalar_t>(1.0), *Ann);

      A->tileTick(n, k);

      delete Ank;
      delete Ann;

      return PARSEC_HOOK_RETURN_DONE;
   }

   // template<typename matrix_t, typename scalar_t>
   // int gemm(parsec_execution_stream_t *es, parsec_task_t *this_task) {
   //    (void)es;

   //    void *Aptr, *Amnptr, *Amkptr, *Ankptr;
   //    int64_t m, n, k;
   //    matrix_t *A;

   //    std::cout << "[gemm]" << std::endl;

   //    parsec_dtd_unpack_args(this_task, &Aptr, &Amnptr, &Amkptr, &Ankptr, &m, &n, &k);

   //    A = static_cast< matrix_t* >(Aptr);
   //    auto Amn = static_cast< slate::Tile<scalar_t>* >(Amnptr);
   //    auto Amk = static_cast< slate::Tile<scalar_t>* >(Amkptr);
   //    auto Ank = static_cast< slate::Tile<scalar_t>* >(Ankptr);
   //    // std::cout << "K: "<<k<<" GEMM("<<m<<";"<<n<<")" << std::endl;
   //    slate::gemm<scalar_t>(scalar_t(-1.0), *Amk, *Ank, scalar_t(1.0), *Amn);
   //    A->tileTick(m, k);
   //    A->tileTick(n, k);

   //    delete Amn;
   //    delete Amk;
   //    delete Ank;

   //    return PARSEC_HOOK_RETURN_DONE;
   // }

   // template<typename matrix_t, typename scalar_t>
   // int herk_submat(parsec_execution_stream_t *es, parsec_task_t *this_task) {
   //    (void)es;

   //    void *Aptr;
   //    matrix_t *A;
   //    int64_t l, k;
   //    int32_t *column_k, *column_l, *column_nt;

   //    std::cout << "[herk_submat]" << std::endl;

   //    parsec_dtd_unpack_args(this_task, &column_k, &column_l, &column_nt, &Aptr, &k, &l);

   //    A = static_cast< matrix_t* >(Aptr);

   //    A->parsec_trailing_matrix_tp = parsec_dtd_taskpool_new();

   //    A->enqueue_taskpool(A->parsec_trailing_matrix_tp);

   //    // parsec_taskpool_set_complete_callback(
   //    //       A->parsec_trailing_matrix_tp,
   //    //       parsec::slate_parsec_complete_tp_callback,
   //    //       (void *)this_task);

   //    for (int64_t n = l; n < A->nt(); ++n) {

   //       if (A->tileIsLocal(n, n)) {

   //          auto Ann = new slate::Tile<scalar_t>(A->at( n, n));
   //          auto Ank = new slate::Tile<scalar_t>(A->at( n,k));

   //          parsec_dtd_taskpool_insert_task(
   //                A->parsec_trailing_matrix_tp,
   //                parsec::herk<matrix_t, scalar_t>, this_task->priority,
   //                "potrf_herk",
   //                sizeof(void*),   Aptr, SCRATCH,
   //                sizeof(void*),   Ann,  SCRATCH,
   //                sizeof(void*),   Ank,  SCRATCH,
   //                sizeof(int64_t), &k,   VALUE,
   //                sizeof(int64_t), &n,   VALUE,
   //                PARSEC_DTD_ARG_END);

   //       }

   //       for (int64_t m = n+1; m < A->nt(); ++m)

   //          if (A->tileIsLocal(m, n)) {

   //             auto Amn = new slate::Tile<scalar_t>(A->at(m, n));
   //             auto Amk = new slate::Tile<scalar_t>(A->at(m, k));

   //             auto ank = A->sub(n, n, k, k);
   //             auto conj_ank = conj_transpose(ank);
   //             auto Ank = new slate::Tile<scalar_t>(conj_ank.at(0,0));

   //             std::cout << "[herk_submat] TETETET 2" << std::endl;

   //             parsec_dtd_taskpool_insert_task(
   //                   A->parsec_trailing_matrix_tp,
   //                   parsec::gemm<matrix_t, scalar_t>, this_task->priority,
   //                   "potrf_gemm",
   //                   sizeof(void*),   Aptr, SCRATCH,
   //                   sizeof(void*),   Amn,  SCRATCH,
   //                   sizeof(void*),   Amk,  SCRATCH,
   //                   sizeof(void*),   Ank,  SCRATCH,
   //                   sizeof(int64_t), &m,   VALUE,
   //                   sizeof(int64_t), &n,   VALUE,
   //                   sizeof(int64_t), &k,   VALUE,
   //                   PARSEC_DTD_ARG_END);
   //          }
   //    }


   //    parsec_dtd_taskpool_wait(A->parsec_trailing_matrix_tp);

   //    parsec_dtd_dequeue_taskpool(
   //          A->parsec_trailing_matrix_tp);

   //    parsec_taskpool_free(A->parsec_trailing_matrix_tp);

   //    // parsec_dtd_dequeue_taskpool(
   //    //       A->parsec_trailing_matrix_tp,
   //    //       A->parsec_trailing_matrix_tp->context);
   //    // parsec_dtd_dequeue(A->parsec_trailing_matrix_tp->context, A->parsec_trailing_matrix_tp);

   //    // return PARSEC_HOOK_RETURN_ASYNC;
   //    return PARSEC_HOOK_RETURN_DONE;
   // }

   // would BaseMatrix be enough to assume. Do we need to have the HermitianMatrix type?
   // template<typename matrix_t, typename scalar_t>
   // int herk_panel(parsec_execution_stream_t *es, parsec_task_t *this_task) {
   //    (void)es;

   //    void *Aptr;
   //    matrix_t *A;
   //    int64_t k, n;
   //    int32_t *column_k, *column_n;

   //    parsec_dtd_unpack_args(
   //          this_task,
   //          &column_k,
   //          &column_n,
   //          &Aptr,
   //          &k, &n);

   //    int l = n - (k+1);

   //    A = static_cast< matrix_t* >(Aptr);

   //    // A->parsec_lookahead_tp[l] = parsec_dtd_taskpool_new();
   //    parsec_taskpool_t *taskpool = parsec_dtd_taskpool_new();
      
   //    A->enqueue_taskpool(taskpool);

   //    // This task does not complete.
   //    // We register its completion callback on the taskpool completion
   //    parsec_taskpool_set_complete_callback(
   //          taskpool,
   //          slate_parsec_complete_tp_callback,
   //          (void *)this_task);

   //    if (A->tileIsLocal(n,n)) {

   //       auto Ann = new slate::Tile<scalar_t>(A->at(n,n));
   //       auto Ank = new slate::Tile<scalar_t>(A->at(n,k));

   //       parsec_dtd_taskpool_insert_task(
   //             taskpool,
   //             parsec::herk<matrix_t, scalar_t>, this_task->priority,
   //             "potrf_herk",
   //             sizeof(void*),   Aptr, SCRATCH,
   //             sizeof(void*),   Ann,  SCRATCH,
   //             sizeof(void*),   Ank,  SCRATCH,
   //             sizeof(int64_t), &k,   VALUE,
   //             sizeof(int64_t), &n,   VALUE,
   //             PARSEC_DTD_ARG_END);
   //    }

   //    for (int64_t m = n+1; m < A->nt(); ++m) {
         
   //       if (A->tileIsLocal(m,n)) {

   //          auto Amn = new slate::Tile<scalar_t>(A->at( m,n));
   //          auto Amk = new slate::Tile<scalar_t>(A->at( m,k));

   //          auto ank = A->sub(n,n,k,k);
   //          auto conj_ank = conj_transpose(ank);
   //          auto Ank = new slate::Tile<scalar_t>(conj_ank.at(0,0));

   //          parsec_dtd_taskpool_insert_task(
   //                taskpool,
   //                parsec::gemm<matrix_t, scalar_t>, this_task->priority,
   //                "potrf_gemm",
   //                sizeof(void*),   Aptr, SCRATCH,
   //                sizeof(void*),   Amn,  SCRATCH,
   //                sizeof(void*),   Amk,  SCRATCH,
   //                sizeof(void*),   Ank,  SCRATCH,
   //                sizeof(int64_t), &m,   VALUE,
   //                sizeof(int64_t), &n,   VALUE,
   //                sizeof(int64_t), &k,   VALUE,
   //                PARSEC_DTD_ARG_END);
   //       }
   //    }
      
   //    parsec_dtd_dequeue_taskpool(taskpool);

   //    // parsec_dtd_taskpool_wait(A->parsec_context, taskpool);

   //    // parsec_taskpool_free(taskpool);

   //    // parsec_dtd_dequeue_taskpool(
   //    //       A->parsec_lookahead_tp[l],
   //    //       A->parsec_lookahead_tp[l]->context);
   //    // parsec_dtd_dequeue(A->parsec_lookahead_tp[l]->context, A->parsec_lookahead_tp[l]);

   //    // return PARSEC_HOOK_RETURN_DONE;
   //    return PARSEC_HOOK_RETURN_ASYNC;
   // }

   /* The four high level tasks that manage dependencies

      - panel read and write column k. It inserts 1 PO for Akk, and 1
      TRSM for all Amk below Akk using Akk.

      - multicast of Amk to the right for the update, read column k,
      read/write k+1

      - lookahead read column k, read and write column n. It inserts a
      HERK for Ann using Ank. It will look into the hashtable for
      Ank availability. It inserts GEMM for each Amn below Ann using
      Ank and Amk.

      - trailing_submatrix read column k, read and write on interval
      column [k+1+lookahead ; nt-1] which is simplified in its
      boundaries. It applies a *huge* HERK somehow.
   */

   // template<typename matrix_t, typename scalar_t>
   // int parsec_copy_data(parsec_execution_stream_t *es, parsec_task_t *this_task) {
      
   //      void *Aptr;
   //      matrix_t *A;
   //      int64_t i, j;
   //      int rank;
   //      int32_t life;
   //      void *data;
   //      int32_t root;

   //      parsec_dtd_unpack_args(this_task, &data, &Aptr, &rank, &life, &root, &i, &j);

   //      A = static_cast< matrix_t* >(Aptr);

   //      std::cout << "[parsec_copy_data] TETETETETETETE" << std::endl; 
        
   //      /* get a new tile and memcpy on data */
   //      if(rank != root) {
   //          // slate::Tile<scalar_t> *Akk = new slate::Tile<scalar_t>(A->at(i,j));
   //          // memcpy(Akk->data(), data, Akk->bytes());

   //         std::cout << "[P"<<rank<<"] Copying data after receving tile (" << i << ";" << j << ") from [P"<<root<<"] data[0] = " << *(scalar_t*)data << "; "  << ((scalar_t*)data)[1] << "; "  << ((scalar_t*)data)[2] << "; "  << ((scalar_t*)data)[3] <<std::endl;
   //          auto tile = A->tileInsert(i, j);
   //          A->tileLife(i, j, life);
   //          memcpy(tile->data(), data, tile->bytes()); //!!! bytes should be what we're looking for. I leave a mark to come back here and check
   //      } else {
   //          std::cout << "[P"<<rank<<"] Copying data after receving tile (" << i << ";" << j << ") from [P"<<root<<"] data[0] = " << *(scalar_t*)data << "; "  << ((scalar_t*)data)[1] << "; "  << ((scalar_t*)data)[6] << "; "  << ((scalar_t*)data)[7] <<std::endl;
   //      }
        
   //      return PARSEC_HOOK_RETURN_DONE;
   // }
   

   template<typename matrix_t, typename scalar_t>
   int tile_send(
         parsec_execution_stream_t *es,
         parsec_task_t *this_task) {

      void *Aik_data = nullptr;
      // void *Aij_data = nullptr;
      void *A_ptr = nullptr;
      int64_t k;
      int64_t i;
      int64_t j;
      int Aij_rank;
      // void *column_k;
      
      parsec_dtd_unpack_args(
            this_task,
            /*&column_k,*/ &Aik_data/*, &Aij_data*/, &A_ptr, &k, &i, &j, &Aij_rank);

      matrix_t *A = static_cast<matrix_t*>(A_ptr);
      auto rank = A->mpiRank();

      scalar_t Aik_data_val = scalar_t(0); 
      if (Aik_data) {
         Aik_data_val = ((scalar_t*)Aik_data)[0];
      }

      auto Aik_tile = A->at(i,k);
      
      std::memcpy((void*)Aik_tile.data(), Aik_data, Aik_tile.bytes());
      
      std::cout << "[tile_send] myrank = " << rank 
                << ", (i,k) = (" << i << "," << k << ")"
                << ", Aij_rank = " << Aij_rank
                << ", Aik_tile(0,0) = " << Aik_tile(0,0)
                << ", data(0,0) = " << Aik_data_val
                << std::endl;

      return PARSEC_HOOK_RETURN_DONE;
   }

   template<typename matrix_t, typename scalar_t>
   void tile_send_task(
         matrix_t& A, int64_t k, int64_t i, int64_t j) {

      // Taskpool
      parsec_taskpool_t *taskpool = A.parsec_high_level_tp;
      // parsec_taskpool_t *taskpool = A.parsec_high_level_tp;
      
      // parsec_taskpool_t *taskpool = parsec_dtd_taskpool_new();
      // Enqueue taskpool
      // A.enqueue_taskpool(taskpool);

      // FIXME: Is this alias necessary?
      void *A_ptr = &A;

      // int arena_index;
      // if (A.tileRank(i, j) == A.mpiRank()) {

      int64_t life = 0;
      // life += A_sub_row.numLocalTiles();
      // life += A_sub_col.numLocalTiles();

      life = A.numLocalTiles();
      // FIXME: is tileExists Thread-safe?
      if(!A.tileExists(i, k)) {
         auto* tile = A.tileInsertWorkspace(i, k);
         std::cout << "[tile_send] my rank = " << A.mpiRank()
                   << ", insert (" << i << "," << k << ") tile " << std::endl;
      }
      else {
         // If tile already exists, add to its life span.
         life += A.tileLife(i, k);
         std::cout << "[tile_send] my rank = " << A.mpiRank()
                   << ", tile (" << i << "," << k << ") already exists " << std::endl;
      }
      A.tileLife(i, k, life);

         
      auto* Aik_dtd_tile = A.desc->dtd_tiles[A.desc->super.data_key(&(A.desc->super), i, k)];
      // auto* Aij_dtd_tile = A.desc->dtd_tiles[A.desc->super.data_key(&(A.desc->super), i, j)];

      int aik_arena_index = (A.tileRank(i, k) == A.mpiRank()) ? parsec::LAPACK_TILE : parsec::FULL_TILE;
      // int aij_arena_index = (A.tileRank(i, j) == A.mpiRank()) ? parsec::LAPACK_TILE : parsec::FULL_TILE;

      int Aij_rank = A.tileRank(i, j);
         
      parsec_dtd_taskpool_insert_task(
            taskpool,
            parsec::tile_send<matrix_t, scalar_t>, 0,
            "TileSend",
            // PASSED_BY_REF, A.column(k), INOUT,
            // PASSED_BY_REF, NULL, INPUT | aik_arena_index,
            PASSED_BY_REF, Aik_dtd_tile, INPUT | aik_arena_index,
            // PASSED_BY_REF, Aij_dtd_tile, INOUT | aij_arena_index | AFFINITY,
            sizeof(void*), A_ptr, SCRATCH,
            sizeof(int64_t), &k, VALUE,
            sizeof(int64_t), &i, VALUE,
            sizeof(int64_t), &j, VALUE,
            sizeof(int), &Aij_rank, VALUE | AFFINITY,
            PARSEC_DTD_ARG_END
            );      


      // std::cout << "[tile_bcast] my rank = " << mpi_rank << ", tile_bcast WAITING.. " << std::endl;
      // std::cout << "[tile_bcast] my rank = " << mpi_rank << ", tile_bcast DEQUEUING.. " << std::endl;

         
      // parsec_dtd_data_flush(taskpool, Aij_dtd_tile);
      parsec_dtd_data_flush(taskpool, Aik_dtd_tile);

      parsec_dtd_taskpool_wait(taskpool);

      // }

      // parsec_dtd_dequeue_taskpool(
            // taskpool, taskpool->context);

      // parsec_taskpool_free(taskpool);

   }

   template<typename matrix_t, typename scalar_t>
   int column_tile_send(
         parsec_execution_stream_t *es,
         parsec_task_t *this_task) {

      int32_t *column_k, *column_j;
      void *A_ptr = nullptr;
      int64_t k;
      int64_t j;

      parsec_dtd_unpack_args(
            this_task,
            &column_k, &column_j, &A_ptr, &k, &j);
      
      matrix_t *A = static_cast<matrix_t*>(A_ptr);
      auto rank = A->mpiRank();

      std::cout << "[column_tile_send] myrank = " << rank 
                << ", (k,j) = (" << k << "," << j << ")"
                // << ", column_k = " << column_k
                // << ", column_j = " << column_j
                << std::endl;

      // return PARSEC_HOOK_RETURN_DONE;
      // parsec_taskpool_t *taskpool = A->parsec_high_level_tp;
      // parsec_taskpool_t *taskpool = A->parsec_tileSend_tp;

      parsec_taskpool_t *taskpool = parsec_dtd_taskpool_new();
      // Enqueue taskpool
      A->enqueue_taskpool(taskpool);

      for (int64_t i = k+1; i < A->nt(); ++i) {

         int64_t life = 0;
         life = 1;

         if(!A->tileExists(i, k)) {
            auto* tile = A->tileInsertWorkspace(i, k);
            // auto* tile = A->tileInsert (i, k);
            // tile_data_register(A->desc, i, k, tile->data());

            // parsec_data_t* parsec_data = A->desc->super.data_of((parsec_data_collection_t*)A->desc, i, k);
            // parsec_data->device_copies[0]->device_private = NULL;

            std::cout << "[column_tile_send] my rank = " << A->mpiRank()
                      << ", insert (" << i << "," << k << ") with data =  " << tile->data() << std::endl;
         }
         else {
            // If tile already exists, add to its life span.
            life += A->tileLife(i, k);
            auto tile = A->at(i,k);
            std::cout << "[column_tile_send] my rank = " << A->mpiRank()
                      << ", tile (" << i << "," << k << ") exists"
                      << ", Aik_tile(0,0) = " << tile(0,0)
                      << std::endl;
         }
         A->tileLife(i, k, life);

         // auto* Aik_dtd_tile = A->desc->dtd_tiles[A->desc->super.data_key(&(A->desc->super), i, k)];
         // auto* Aij_dtd_tile = A.desc->dtd_tiles[A.desc->super.data_key(&(A.desc->super), i, j)];

         auto Aij_dtd_key = A->desc->super.data_key(&(A->desc->super), i, j);
         auto Aik_dtd_key = A->desc->super.data_key(&(A->desc->super), i, k);

         auto* Aij_dtd_tile = PARSEC_DTD_TILE_OF_KEY(&(A->desc->super), Aij_dtd_key);
         auto* Aik_dtd_tile = PARSEC_DTD_TILE_OF_KEY(&(A->desc->super), Aik_dtd_key);
            
         int aij_arena_index = (A->tileRank(i, j) == A->mpiRank()) ? parsec::LAPACK_TILE : parsec::FULL_TILE;
         int aik_arena_index = (A->tileRank(i, k) == A->mpiRank()) ? parsec::LAPACK_TILE : parsec::FULL_TILE;

         int Aij_rank = A->tileRank(i, j);

         parsec_dtd_taskpool_insert_task(
               taskpool,
               parsec::tile_send<matrix_t, scalar_t>, 0,
               "TileSendAik",
               // PASSED_BY_REF, A.column(k), INOUT,
               // PASSED_BY_REF, NULL, INPUT | aik_arena_index,
               // PASSED_BY_REF, Aik_dtd_tile, INOUT | aik_arena_index,
               PASSED_BY_REF, Aik_dtd_tile, INPUT | aik_arena_index,
               // PASSED_BY_REF, Aij_dtd_tile, INOUT | aij_arena_index | AFFINITY,
               sizeof(void*), A_ptr, SCRATCH,
               sizeof(int64_t), &k, VALUE,
               sizeof(int64_t), &i, VALUE,
               sizeof(int64_t), &j, VALUE,
               sizeof(int), &Aij_rank, VALUE | AFFINITY,
               PARSEC_DTD_ARG_END
               );      

         parsec_dtd_data_flush(taskpool, Aik_dtd_tile);
         std::cout << "[column_tile_send] my rank = " << A->mpiRank()
                   << ", flushed Aik" << std::endl;

         
      }

      std::cout << "[column_tile_send] my rank = " << A->mpiRank()
                << ", waiting for TileSendAik" << std::endl;
      parsec_dtd_taskpool_wait(taskpool);

      parsec_dtd_dequeue_taskpool(taskpool);
      parsec_taskpool_free(taskpool);
      
      return PARSEC_HOOK_RETURN_DONE;
   }
   
   template<typename matrix_t, typename scalar_t>
   void column_tile_send_task(
         matrix_t& A, int64_t k, int64_t j) {

      // Taskpool
      parsec_taskpool_t *taskpool = A.parsec_high_level_tp;
      // FIXME: Is this alias necessary?
      void *A_ptr = &A;

      // auto column_k_key = A.columns->super.data_key(&(A.columns->super), k);
      // auto column_j_key = A.columns->super.data_key(&(A.columns->super), j);

      // std::cout << "[column_tile_send_task] k = " << k << std::endl; 
      // std::cout << "[column_tile_send_task] column " << k
      //           << ", myrank = " << A.mpiRank()
      //           << ", column rank of = " << A.columns->super.rank_of_key(&(A.columns->super), column_k_key)
      //           << std::endl; 
      // std::cout << "[column_tile_send_task] column " << j
      //           << ", myrank = " << A.mpiRank()
      //           << ", column rank of  = " << A.columns->super.rank_of_key(&(A.columns->super), column_j_key)
      //           << std::endl; 

      parsec_dtd_taskpool_insert_task(
            taskpool,
            parsec::column_tile_send<matrix_t, scalar_t>, 0,
            "ColumnSendA:j",
            PASSED_BY_REF, A.column(k), INOUT,
            PASSED_BY_REF, A.column(j), INOUT | AFFINITY,
            sizeof(void*), A_ptr, SCRATCH,
            sizeof(int64_t), &k, VALUE,
            sizeof(int64_t), &j, VALUE,
            PARSEC_DTD_ARG_END);      


      // parsec_dtd_taskpool_wait(A.parsec_context, taskpool);

   }

   template<typename matrix_t, typename scalar_t>
   int column_dummy(
         parsec_execution_stream_t *es,
         parsec_task_t *this_task) {

      int32_t *column_k;
      int k;
      void *A_ptr = nullptr;

      parsec_dtd_unpack_args(
            this_task, &column_k, &k, &A_ptr);

      std::cout << "[column_dummy] k = " << k << std::endl; 

      return PARSEC_HOOK_RETURN_DONE;
   }

   template<typename matrix_t, typename scalar_t>
   void column_dummy_task(
         matrix_t& A, int64_t k) {

      // Taskpool
      parsec_taskpool_t *taskpool = A.parsec_high_level_tp;
      // FIXME: Is this alias necessary?
      void *A_ptr = &A;

      parsec_dtd_tile_t *column_k = A.column(k);
          
      parsec_dtd_taskpool_insert_task(
            taskpool,
            parsec::column_dummy<matrix_t, scalar_t>, 0,
            "ColumnDummy",
            PASSED_BY_REF, column_k, INOUT | AFFINITY,
            sizeof(int64_t), &k, VALUE,
            sizeof(void*), A_ptr, SCRATCH,
            PARSEC_DTD_ARG_END);
   }
   
#if 0
   template<typename matrix_t, typename scalar_t>
   int tile_bcast(
         parsec_execution_stream_t *es,
         parsec_task_t *this_task) {

      void *A_ptr = nullptr;
      int64_t k = -1;
      
      parsec_dtd_unpack_args(this_task, &A_ptr, &k);

      matrix_t *A = static_cast<matrix_t*>(A_ptr);

      // std::cout << "[tile_bcast] A_ptr = " << A_ptr << std::endl;
      // std::cout << "[tile_bcast] k = " << k << std::endl;
      std::cout << "[tile_bcast] Is (k,k) local = " << A->tileIsLocal(k, k) << std::endl;

      // parsec_taskpool_t *comm_tp = parsec_dtd_taskpool_new();
      // Enaueue taskpool
      // A->enqueue_taskpool(comm_tp);

      // parsec_dtd_taskpool_insert_task(
      //       comm_tp,
      //       dummy_kernel,
      //       0, /* Priority */
      //       "dummy",
      //       // We create the dependency to wait for the communication
      //       PARSEC_DTD_ARG_END);

      
      // parsec_dtd_taskpool_wait(A->parsec_context, comm_tp);
      // parsec_dtd_dequeue_taskpool(comm_tp, comm_tp->context);
      // parsec_taskpool_free(comm_tp);

      auto mpi_rank = A->mpiRank();
      
      // Number of block columns
      auto const A_nt = A->nt();
      // Submatrix where (k,k) tile is being broadcasted
      // auto const A_sub = A->sub(k+1, A_nt-1, k, k);

      for (int64_t i = k+1; i < A_nt; ++i) {

         // Root tile
         int root_rank = A->tileRank(i, k);

         auto const A_sub_row = A->sub(i, i, k+1, i);
         auto const A_sub_col = A->sub(i, A_nt-1, i, i);
         std::set<int> bcast_set;
         // Find the set of participating ranks.
         bcast_set.insert(A->tileRank(i, k)); // Insert root.

         // Insert destinations
         
         // Block row `i`
         A_sub_row.getRanks(&bcast_set); // Insert destinations.
         // Block column `i`
         A_sub_col.getRanks(&bcast_set); // Insert destinations.

         if (mpi_rank == 0) {
            std::cout << "[tile_bcast] Root (" << i << "," << k << ")"  << std::endl;
            std::cout << "[tile_bcast] Number of participating ranks: " << bcast_set.size() << std::endl;
            for (auto rank: bcast_set) {
               std::cout << "[tile_bcast] Participating rank = " << rank << std::endl;
            }
         }

         // If this rank is in the set.
         if (bcast_set.find(mpi_rank) != bcast_set.end()) {

            std::cout << "[tile_bcast] my rank = " << mpi_rank << " participating in the bcast" << std::endl;

            // If receiving the tile.
            if (! A->tileIsLocal(i, k)) {

               parsec_remote_deps_t *deps = nullptr;
               int arena_index;
               deps = (parsec_remote_deps_t*)remote_deps_allocate(&parsec_remote_dep_context.freelist);
               assert(root_rank == A->desc->super.rank_of(&(A->desc->super), i, k));
               deps->root = root_rank; // this->desc->super.rank_of(&(this->desc->super), i, k);
               deps->outgoing_mask = (1 << 0);
               deps->max_priority  = !!(mpi_rank == deps->root);               
               
               int64_t life = 0;
               life += A_sub_row.numLocalTiles();
               life += A_sub_col.numLocalTiles();

               // FIXME: is tileExists Thread-safe?
               if(!A->tileExists(i, k)) {
                  auto* tile = A->tileInsertWorkspace(i, k);
                  std::cout << "[tile_bcast] my rank = " << mpi_rank
                            << ", insert (" << i << "," << k << ") tile " << std::endl;
               }
               else {
                  // If tile already exists, add to its life span.
                  life += A->tileLife(i, k);
                  std::cout << "[tile_bcast] my rank = " << mpi_rank
                            << ", tile (" << i << "," << k << ") already exists " << std::endl;
               }
               A->tileLife(i, k, life);

               struct remote_dep_output_param_s* output;
               output = &deps->output[0];
               output->data.data = A->desc->super.data_of(&(A->desc->super), i, k)->device_copies[0];
               assert(output->data.data->device_private != NULL);

               if (deps->root == mpi_rank) {
                  // Original matrix storage
                  output->data.arena = parsec_dtd_arenas[parsec::LAPACK_TILE];
               }
               else {
                  // Workspace storage
                  output->data.arena = parsec_dtd_arenas[parsec::FULL_TILE];
               }
               output->data.layout = output->data.arena->opaque_dtt;// if root, scalapack+lda; else tile mb*nb, lda = mb;
             
               output->data.count  = 1;
               output->data.displ  = 0;
               output->priority    = 0;

               int _array_pos, _array_mask;

               for (auto rank: bcast_set) {
                  
                  if (rank == root_rank) continue;
                  
                  if (rank == mpi_rank) {  
                     deps->max_priority++;
                  }

                  _array_pos = rank / (8 * sizeof(uint32_t));
                  _array_mask = 1 << (rank % (8 * sizeof(uint32_t)));
                     
                  if( !(output->rank_bits[_array_pos] & _array_mask) ) {
                     output->rank_bits[_array_pos] |= _array_mask;
                     output->deps_mask |= (1 << 0);
                     output->count_bits++;
                  }  /* otherwise the bit is already flipped, the peer is already part of the propagation. */

               }      

               parsec_taskpool_t *taskpool = parsec_dtd_taskpool_new();
               // Enqueue taskpool
               A->enqueue_taskpool(taskpool);

               std::cout << "[tile_bcast] my rank = " << mpi_rank
                         << ", deps->max_priority = " << deps->max_priority << std::endl;

               if ( (0 == deps->max_priority) ||
                    ((deps->root == mpi_rank) && (0 == deps->output[0].count_bits)) ) {

                  deps->pending_ack = 0;
                  deps->incoming_mask = 0;
                  deps->outgoing_mask = 0;
                  remote_deps_free(deps);
                  deps = nullptr;

                  std::cout << "[tile_bcast] my rank = " << mpi_rank
                            << ", nothing to send " << std::endl;

                  
                  parsec_dtd_increment_id(taskpool, 2);

               }
               else {
                  
                  deps->taskpool = taskpool;
                  
                  int arena_index;
                  if (deps->root == mpi_rank) {
                     arena_index = parsec::LAPACK_TILE;
                  } else {
                     arena_index = parsec::FULL_TILE;
                  }

                  std::cout << "[tile_bcast] SENDING DATA" << std::endl;

                  // parsec_data_collection_t *data_collection = (parsec_data_collection_t *)A->desc;
                  // auto Aik_key = data_collection->data_key(data_collection, i, k);

                  auto* Aik_dtd_tile = A->desc->dtd_tiles[A->desc->super.data_key(&(A->desc->super), i, k)];
                  // auto* Aik_dtd_tile = PARSEC_DTD_TILE_OF_KEY(data_collection, Aik_key);

                  parsec_dtd_taskpool_insert_task(
                        taskpool, parsec_copy_data<matrix_t, scalar_t>, 0,
                        "Copy_data",
                        PASSED_BY_REF, Aik_dtd_tile, INPUT | arena_index,
                        sizeof(void*), A_ptr, SCRATCH,
                        sizeof(int), &mpi_rank, VALUE | AFFINITY,
                        sizeof(int), &deps->max_priority, VALUE,
                        sizeof(int), &deps->root, VALUE,
                        sizeof(int64_t), &i, VALUE,
                        sizeof(int64_t), &k, VALUE,
                        PARSEC_DTD_ARG_END);
                  
                  // parsec_dtd_insert_replicate_task(
                  //       taskpool, deps,
                  //       Aik_dtd_tile,
                  //       mpi_rank | AFFINITY, arena_index, 0);

                  parsec_dtd_data_flush(taskpool, Aik_dtd_tile);
               }

               std::cout << "[tile_bcast] my rank = " << mpi_rank << ", tile_bcast WAITING.. " << std::endl;
               parsec_dtd_taskpool_wait(taskpool);
               std::cout << "[tile_bcast] my rank = " << mpi_rank << ", tile_bcast DEQUEUING.. " << std::endl;

               parsec_dtd_dequeue_taskpool(taskpool);

               parsec_taskpool_free(taskpool);

            }
            else {

               std::cout << "[tile_bcast] my rank = " << mpi_rank
                         << ", tile (" << i << "," << k << ") is local " << std::endl;
            }

            std::cout << "[tile_bcast] my rank = " << mpi_rank << ", tile_bcast DONE! " << std::endl;
         }

      }
            
      // auto tile = this->tileInsert(i, j, A->host_num_);
      // // Set life counter
      // int64_t life = 1;
      // this->tileLife(i, j, life);

      
      return PARSEC_HOOK_RETURN_DONE;
   }

   template<typename matrix_t, typename scalar_t>
   void tile_bcast_task(
         matrix_t& A, int64_t k) {

      matrix_t *Aptr = &A;
      uint64_t priority = 0;

      // Taskpool
      parsec_taskpool_t *tp = A.parsec_high_level_tp;
      // FIXME: Is this alias necessary?
      void *A_ptr = &A;
      
      parsec_dtd_taskpool_insert_task(
            tp,            
            parsec::tile_bcast<matrix_t, scalar_t>, priority,
            "TileBcast",
            sizeof(void*), A_ptr, SCRATCH,
            sizeof(int64_t), &k, VALUE,
            PARSEC_DTD_ARG_END
            );
   }
#endif
   
   template<typename matrix_t, typename scalar_t>
   void potrf_task(matrix_t& A, int64_t k) {
      
      std::cout << "[potrf_task]" << std::endl;

      if (!A.tileIsLocal(k, k))
         return;

      matrix_t *Aptr = &A;
      uint64_t priority = 0;
      
      slate::Tile<scalar_t> *Akk = new slate::Tile<scalar_t>(A.at(k,k));

      // parsec_dtd_taskpool_insert_task(
      //       A.parsec_high_level_tp,
      //       parsec::potrf<matrix_t, scalar_t>, priority,
      //       "potrf + trsm",
      //       PASSED_BY_REF,      column_k,      INOUT | AFFINITY,
      //       sizeof(int64_t),    &k,            VALUE,
      //       sizeof(void*),      Aptr,          SCRATCH,
      //       PARSEC_DTD_ARG_END);

      parsec_dtd_taskpool_insert_task(
            A.parsec_high_level_tp,
            parsec::potrf<scalar_t>,
            priority, /* inherits the priority from high level task */
            "potrf",
            // We create the dependency to wait for the communication
            sizeof(slate::Tile<scalar_t>*), Akk, SCRATCH,
            sizeof(int64_t), &k, VALUE,
            PARSEC_DTD_ARG_END);

   }
   
   template<typename matrix_t, typename scalar_t>
   void potrf_panel_task(matrix_t& A, int64_t k, parsec_dtd_tile_t *column_k) {

      // std::cout << "[potrf_panel_task]" << std::endl;
      
      void *Aptr = &A;
      uint64_t priority = (A.mt()-k)*(A.mt()-k)*(A.mt()-k);
      parsec_dtd_taskpool_insert_task(
            A.parsec_high_level_tp,
            parsec::potrf_panel<matrix_t, scalar_t>, priority,
            "potrf + trsm",
            PASSED_BY_REF,      column_k,      INOUT | AFFINITY,
            sizeof(int64_t),    &k,            VALUE,
            sizeof(void*),      Aptr,          SCRATCH,
            PARSEC_DTD_ARG_END);
   }

   template<typename matrix_t, typename scalar_t>
   void Amk_comm_task(
         matrix_t& A, int64_t k, parsec_dtd_tile_t *column_k,
         parsec_dtd_tile_t *column_kp1, parsec_dtd_tile_t *column_n) {
      void *Aptr = &A;
      uint64_t priority = (A.mt()-k)*(A.mt()-k)*(A.mt()-k);
      parsec_dtd_taskpool_insert_task(
            A.parsec_high_level_tp,
            parsec::Amk_comm<matrix_t, scalar_t>, priority,
            "Amk Comm Wrapper",
            PASSED_BY_REF,      column_k,      INPUT,
            PASSED_BY_REF,      column_kp1,    INOUT,
            PASSED_BY_REF,      column_n,      INOUT | AFFINITY,
            sizeof(int64_t),    &k,            VALUE,
            sizeof(void*),      Aptr,          SCRATCH,
            PARSEC_DTD_ARG_END);
   }

   // template<typename matrix_t, typename scalar_t>
   // void potrf_trailing_matrix_task(
   //       matrix_t& A, int64_t k, int64_t l,
   //       parsec_dtd_tile_t *column_k,
   //       parsec_dtd_tile_t *column_l,
   //       parsec_dtd_tile_t *column_nt) {
   //    void *Aptr = &A;
   //    uint64_t priority = (A.mt()-l)*(A.mt()-l)*(A.mt()-l)+3*(l-k);
   //    parsec_dtd_taskpool_insert_task(
   //          A.parsec_high_level_tp,
   //          parsec::herk_submat<matrix_t, scalar_t>, priority,
   //          "herk",
   //          PASSED_BY_REF,   column_k,      INPUT,
   //          PASSED_BY_REF,   column_l,      INOUT | AFFINITY,
   //          PASSED_BY_REF,   column_nt,     INOUT,
   //          sizeof(void*),   Aptr,          SCRATCH,
   //          sizeof(int64_t), &k,            VALUE,
   //          sizeof(int64_t), &l,            VALUE,
   //          PARSEC_DTD_ARG_END);
   // }

   // template<typename matrix_t, typename scalar_t>
   // void potrf_lookahead_task(
   //       matrix_t& A, int64_t k, int64_t n,
   //       parsec_dtd_tile_t *column_k, parsec_dtd_tile_t * column_n) {
   //    void *Aptr = &A;
   //    // uint64_t priority = (A.mt()-n)*(A.mt()-n)*(A.mt()-n)+3*(n-k);
   //    uint64_t priority = 0;
   //    /* Lookahead panels, HERK on diagonal, and GEMM on sub-diagonal */
   //    parsec_dtd_taskpool_insert_task(
   //          A.parsec_high_level_tp,
   //          parsec::herk_panel<matrix_t, scalar_t>, priority,
   //          "herk + gemm",
   //          PASSED_BY_REF,   column_k,      INPUT,
   //          PASSED_BY_REF,   column_n,      INOUT | AFFINITY,
   //          sizeof(void*),   Aptr,          SCRATCH,
   //          sizeof(int64_t), &k,            VALUE,
   //          sizeof(int64_t), &n,            VALUE,
   //          PARSEC_DTD_ARG_END);
   // }


}

#endif // PARSEC_TASKS_HH
