#ifndef PARSEC_TILE_BCAST_TASK_HH
#define PARSEC_TILE_BCAST_TASK_HH

#include "slate/parsec/tasks.hh"

namespace parsec {

template<typename matrix_t, typename scalar_t>
class TileBcastTask { // TODO: create proper Gemm Parsec task
public:

   TileBcastTask() { }
      
   struct Arguments {
      // source tile row index
      int64_t i;
      // source tile column index
      int64_t j;
      // Destination tile range
      std::list<std::array<int64_t, 4>> dst_ranges;
         
      // Matrix pointer
      matrix_t *a;

      // Default constructor
      Arguments() :
         i(-1), j(-1), dst_ranges({{-1,-1,-1,-1}}), a(nullptr) { }

      Arguments(int64_t in_i, int64_t in_j, std::array<int64_t, 4> in_dst_range, matrix_t *in_a)
         : i(in_i), j(in_j), dst_ranges({in_dst_range}), a(in_a) { }

      Arguments(int64_t in_i, int64_t in_j, std::list<std::array<int64_t, 4>> in_dst_ranges, matrix_t *in_a)
         : i(in_i), j(in_j), dst_ranges(in_dst_ranges), a(in_a) { }
   };

   void initialize(Arguments const& in_args) {
      this->args_ = Arguments(in_args);
   }

   // Insert task
   // void execute() {

   //    // Source tile
   //    auto i = this->args_.i;
   //    auto j = this->args_.j;
         
   //    cpu_func(i, j, this->args_.dst_range, this->args_.a);
   // }

   void submit(parsec_taskpool_t *tp, int priority = 0) {

      auto i = this->args_.i;
      auto j = this->args_.j;

      auto a = this->args_.a;

      for (auto dst_range : this->args_.dst_ranges) {
            
         // Destination tile range
         auto i1 = dst_range[0]; 
         auto i2 = dst_range[1]; 
         auto j1 = dst_range[2]; 
         auto j2 = dst_range[3]; 

         // std::cout << "[TileBcast::submit] Send A(" << i << ", " << j <<  ")"
         //           << ", to A(" << i1 << ":" << i2 << "," << j1 << ":" << j2 << ") ??"
         //           << std::endl;

         // Look if there are tiles in the input range
         if ((i2 < i1) || (j2 < j1))
            return;

         int mpi_rank = a->mpiRank();
         
         auto submat = a->sub(i1, i2, j1, j2);

         // Retrieve rank list
         std::set<int> bcast_set;
         submat.getRanks(&bcast_set);

         // Find the broadcast root rank.
         int root_rank = a->tileRank(i, j);

         // parsec_taskpool_t *tp = parsec_dtd_taskpool_new();
         // a->enqueue_taskpool(tp);
         // int priority = 0;
         
         // Insert the tasks to send data corresponding to A(i,j) tile
         for (auto bcast_rank : bcast_set) {

            // If data is local to the source tile, nothing to do
            if (bcast_rank == root_rank)
               continue;

            // std::cout << "[TileBcast::submit] my rank = " << a->mpiRank()
            //           << ", Send A(" << i << ", " << j <<  ")"
            //           << ", to A(" << i1 << ":" << i2 << "," << j1 << ":" << j2 << ") at rank = " << bcast_rank
            //           << std::endl;

            // Send tile to bcast_rank
            {
               using TileSendTask = parsec::TileSendTask<matrix_t, scalar_t>;
               typename TileSendTask::Arguments tilesend_args(i, j, bcast_rank, a);

               TileSendTask tilesend_task;
               tilesend_task.initialize(tilesend_args);
               tilesend_task.submit(tp);
               // parsec_dtd_taskpool_wait(a->parsec_context, bcast_tp);

            }
            
         }
      } // Loop over dst_ranges         
   }
      
private:

   Arguments args_;

   // static void cpu_func(int64_t i, int64_t j, std::array<int64_t, 4> dst_range, matrix_t *a) {

   //    // Destination tile range
   //    int i1 = dst_range[0]; 
   //    int i2 = dst_range[1]; 
   //    int j1 = dst_range[2]; 
   //    int j2 = dst_range[3]; 

   //    std::cout << "[TileBcast::submit] Send A(" << i << ", " << j <<  ")"
   //              << ", to A(" << i1 << ":" << i2 << "," << j1 << ":" << j2 << ") ??"
   //              << std::endl;

   //    if ((i2 < i1) || (j2 < j1))
   //       return;
            
   //    int mpi_rank = a->mpiRank();
         
   //    auto submat = a->sub(i1, i2, j1, j2);

   //    // Retrieve rank list
   //    std::set<int> bcast_set;
   //    submat.getRanks(&bcast_set);

   //    // Find the broadcast root rank.
   //    int root_rank = a->tileRank(i, j);

   //    parsec_taskpool_t *tp = parsec_dtd_taskpool_new();
   //    a->enqueue_taskpool(tp);
   //    int priority = 0;
         
   //    // Insert the tasks to send data corresponding to A(i,j) tile
   //    for (auto bcast_rank : bcast_set) {

   //       // If data is local to the source tile, nothing to do
   //       if (bcast_rank == root_rank)
   //          continue;

   //       std::cout << "[TileBcast::submit] my rank = " << a->mpiRank()
   //                 << ", Send A(" << i << ", " << j <<  ")"
   //                 << ", to A(" << i1 << ":" << i2 << "," << j1 << ":" << j2 << ") at rank = " << bcast_rank
   //                 << std::endl;

   //       // Send tile to bcast_rank
   //       {
   //          using TileSendTask = parsec::TileSendTask<matrix_t, scalar_t>;
   //          typename TileSendTask::Arguments tilesend_args(i, j, bcast_rank, a);

   //          TileSendTask tilesend_task;
   //          tilesend_task.initialize(tilesend_args);
   //          tilesend_task.submit(tp);
   //          // parsec_dtd_taskpool_wait(a->parsec_context, bcast_tp);

   //       }
            
   //    }

   //    auto* aij_dtd_tile = a->desc->dtd_tiles[a->desc->super.data_key(&(a->desc->super), i, j)];
   //    parsec_dtd_data_flush(tp, aij_dtd_tile);

   //    parsec_dtd_taskpool_wait(a->parsec_context, tp);

   //    parsec_dtd_dequeue_taskpool(tp, tp->context);

   //    parsec_taskpool_free(tp);
   // }

   static int parsec_cpu_func(
         parsec_execution_stream_t *es, parsec_task_t *this_task) {
         
      int64_t i;
      int64_t j;
      scalar_t *aij_data;
      matrix_t *a;
      int tile_rank;
         
      parsec_dtd_unpack_args(this_task, &i, &j, &aij_data, &a, &tile_rank);

      std::cout << "[TileBcastTask::parsec_cpu_func] my rank = " << a->mpiRank()
                << ", copy A(" << i << ", " << j << ")"
                << ", from rank = " << a->tileRank(i, j)
                << std::endl;                  

      // Local tile
      auto aij = a->at(i, j);

      // Copy data receive from A(i,j) owner to local tile
      memcpy(aij.data(), aij_data, aij.bytes());

      return PARSEC_HOOK_RETURN_DONE;
   }
};
   
}

#endif
