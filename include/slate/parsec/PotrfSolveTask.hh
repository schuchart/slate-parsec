#ifndef POTRF_SOLVE_TASK_HH
#define POTRF_SOLVE_TASK_HH

#include <iostream>

#include "slate/parsec/tasks.hh"
#include "slate/Tile.hh"

namespace parsec {

#ifdef PARSEC_DTD_HAVE_CUDA

template<typename matrix_t, typename scalar_t>
class PotrfSolveGpuTask {
public:
   struct Arguments {
      // Tile row index
      int64_t i;
      // Tile column index
      int64_t k;
      // Matrix pointer
      matrix_t *a;
      // Push out data to CPU device
      bool pushout;
      
      // Default constructor
      Arguments() :
         i(-1), k(-1), a(nullptr), pushout(false) { }

      Arguments(int64_t in_i, int64_t in_k, matrix_t *in_a)
         : i(in_i), k(in_k), a(in_a), pushout(false) { }
   };

   void initialize(Arguments const& in_args) {
      this->args_ = Arguments(in_args);
   }

   // Insert task
   void submit(parsec_taskpool_t *tp, int priority = 0) {

      auto *a = args_.a;
      auto i = args_.i;
      auto k = args_.k;
      // Retrieve rank owning tile a(i, k)
      int tile_rank = this->args_.a->tileRank(i, k);

      // A(k,k) tile
      //
      auto* akk_dtd_tile = a->desc->dtd_tiles[a->desc->super.data_key(&(a->desc->super), k, k)];
      int akk_arena_index = -1;
      // if (a->tileIsLocal(k, k)) {}
      // int akk_arena_index = (a->tileIsLocal(k, k)) ? parsec::LAPACK_TILE : parsec::FULL_TILE;
      akk_arena_index = get_arena_index(a, k, k);
      
      // A(i,k) tile
      //
      auto* aik_dtd_tile = a->desc->dtd_tiles[a->desc->super.data_key(&(a->desc->super), i, k)];
      int aik_arena_index = -1;
      // int aik_arena_index = (a->tileIsLocal(i, k)) ? parsec::LAPACK_TILE : parsec::FULL_TILE;
      aik_arena_index = get_arena_index(a, i, k);

      // Set data access
      int akk_op_type = INPUT | akk_arena_index;
      int aik_op_type = INOUT | aik_arena_index;

      if (this->args_.pushout) {
         // Indicate that we are pushing data to main memory
         aik_op_type |= PUSHOUT;
      }

      if (a->tileIsLocal(i, k)) {
         int cuda_dev_index = parsec::get_tile_device_index(a, i, k);

         // std::cout << "[PotrfUpdateGpuTask::gpu_func]  k = " << k
         //           << ", A(" << i << ", " << j <<  ")"
         //           << ", device index = " << parsec::get_tile_device_index(a, i, j)
         //           << std::endl;
         
         parsec_data_copy_t *data_copy = aik_dtd_tile->data_copy;
         assert(NULL != data_copy);
         parsec_data_t *data = data_copy->original;
         assert(NULL != data);
         parsec_advise_data_on_device(
               data, cuda_dev_index, PARSEC_DEV_DATA_ADVICE_PREFERRED_DEVICE);

      }

      parsec_dtd_taskpool_insert_cuda_task(
            tp,
            parsec_gpu_func,
            priority, /* inherits the priority from high level task */
            "PotrfSolveDataTask",
            sizeof(decltype(i)), &i, VALUE,
            sizeof(decltype(k)), &k, VALUE,
            sizeof(matrix_t *), &args_.a, VALUE,
            PASSED_BY_REF, akk_dtd_tile, akk_op_type,
            PASSED_BY_REF, aik_dtd_tile, aik_op_type,
            sizeof(int), &tile_rank, VALUE | AFFINITY,
            PARSEC_DTD_ARG_END);

   }

private:

   Arguments args_;

   static void gpu_func(
         cublasHandle_t cuhandle, cudaStream_t cuda_stream,
         int64_t i, int64_t k, matrix_t *a,
         scalar_t *dev_akk_data, scalar_t *dev_aik_data) {

      if (a->tileIsLocal(i, k)) {
         // Make sure diagonal tile has been created locally
         // assert(a->tileExists(k, k));
         
         // std::cout << "[PotrfSolveGpuTask::cpu_func]  A(" << i << ", " << k <<  ")" << std::endl;
               
         auto aik = a->at(i, k);

         auto side = blas::Side::Right;
         auto uplo = a->uplo();
         auto trans = blas::Op::Trans;
         auto diag = blas::Diag::NonUnit;

         auto alpha = scalar_t(1.0);

         auto m = aik.mb(); 
         auto n = aik.nb(); 

         auto akk_mb = a->tileMb(k);

         slate_cublas_call(
               cublasTrsm(
                     cuhandle,
                     slate::internal::cublas_side_const(side),
                     slate::internal::cublas_uplo_const(uplo),
                     slate::internal::cublas_op_const(trans),
                     slate::internal::cublas_diag_const(diag),
                     m, n,
                     &alpha,  /* host or device pointer */
                     dev_akk_data, akk_mb,
                     dev_aik_data, m));
         
      }
      
   }

   static int parsec_gpu_func(
         void *gpu_device,
         cudaStream_t cuda_stream, parsec_task_t *this_task) {

      int64_t i;
      int64_t k;
      matrix_t *a;
      scalar_t *akk_data;
      scalar_t *aik_data;
      int tile_rank;
         
      parsec_dtd_unpack_args(this_task, &i, &k, &a, &akk_data, &aik_data, &tile_rank);

      scalar_t *dev_akk_data = (scalar_t *)parsec_dtd_get_dev_ptr(this_task, 0);
      scalar_t *dev_aik_data = (scalar_t *)parsec_dtd_get_dev_ptr(this_task, 1);

      // DEBUG
      // scalar_t x, y;
      // cudaMemcpyAsync(
      //    &x, dev_akk_data, sizeof(scalar_t),
      //    cudaMemcpyDeviceToHost, cuda_stream);
      // cudaMemcpyAsync(
      //    &y, dev_aik_data, sizeof(scalar_t),
      //    cudaMemcpyDeviceToHost, cuda_stream);   
      // cudaStreamSynchronize(cuda_stream);
      
      // printf("[PotrfSolveGpuTask::parsec_gpu_func] akk = %.12f, aik = %.12f\n", x, y);
      // END DEBUG
      
      cublasStatus_t status;
      cublasHandle_t cuhandle = NULL;

      // status = cublasCreate(&cuhandle);
      // status = cublasSetStream(cuhandle, cuda_stream);

      cuhandle = parsec::get_cublas_handle(cuda_stream);

      assert(NULL != cuhandle);

      gpu_func(
            cuhandle, cuda_stream,
            i, k, a, dev_akk_data, dev_aik_data);

      // DEBUG
      // scalar_t z;
      // cudaMemcpyAsync(
      //    &z, dev_aik_data, sizeof(scalar_t),
      //    cudaMemcpyDeviceToHost, cuda_stream);   
      // cudaStreamSynchronize(cuda_stream);

      // printf("[PotrfSolveGpuTask::parsec_gpu_func] lik = %.12f\n", z);
      // END DEBUG

      return PARSEC_HOOK_RETURN_DONE;

   }
   
};
   
#endif
   
   //
   // PotrfSolve Parsec task with data dependency tracking
   //

template<typename matrix_t, typename scalar_t>
class PotrfSolveDataTask {
public:
   struct Arguments {
      // Tile row index
      int64_t i;
      // Tile column index
      int64_t k;
      // Matrix pointer
      matrix_t *a;
      // Pull in data from GPU device
      bool pullin;
      
      // Default constructor
      Arguments() :
         i(-1), k(-1), a(nullptr), pullin(false) { }

      Arguments(int64_t in_i, int64_t in_k, matrix_t *in_a)
         : i(in_i), k(in_k), a(in_a), pullin(false) { }
   };

   void initialize(Arguments const& in_args) {
      this->args_ = Arguments(in_args);
   }

   // Insert task
   void submit(parsec_taskpool_t *tp, int priority = 0) {

      auto *a = args_.a;
      auto i = args_.i;
      auto k = args_.k;
      // Retrieve rank owning tile a(i, k)
      int tile_rank = this->args_.a->tileRank(i, k);

      // A(k,k) tile
      //
      auto* akk_dtd_tile = a->desc->dtd_tiles[a->desc->super.data_key(&(a->desc->super), k, k)];
      int akk_arena_index = -1;
      // if (a->tileIsLocal(k, k)) {}
      // int akk_arena_index = (a->tileIsLocal(k, k)) ? parsec::LAPACK_TILE : parsec::FULL_TILE;
      akk_arena_index = get_arena_index(a, k, k);
      
      // A(i,k) tile
      //
      auto* aik_dtd_tile = a->desc->dtd_tiles[a->desc->super.data_key(&(a->desc->super), i, k)];
      int aik_arena_index = -1;
      // int aik_arena_index = (a->tileIsLocal(i, k)) ? parsec::LAPACK_TILE : parsec::FULL_TILE;
      aik_arena_index = get_arena_index(a, i, k);

      int akk_op_type = INPUT | akk_arena_index;
      int aik_op_type = INOUT | aik_arena_index;

#ifdef PARSEC_DTD_HAVE_CUDA
      if (this->args_.pullin) {
         // Indicate that we are pulling data from GPU device
         aik_op_type |= PULLIN;
      }
#endif
      
      parsec_dtd_taskpool_insert_task(
            tp,
            parsec_cpu_func,
            priority, /* inherits the priority from high level task */
            "PotrfSolveDataTask",
            sizeof(decltype(i)), &i, VALUE,
            sizeof(decltype(k)), &k, VALUE,
            sizeof(matrix_t *), &args_.a, VALUE,
            PASSED_BY_REF, akk_dtd_tile, akk_op_type,
            PASSED_BY_REF, aik_dtd_tile, aik_op_type,
            sizeof(int), &tile_rank, VALUE | AFFINITY,
            PARSEC_DTD_ARG_END);

   }

private:

   Arguments args_;

   static void cpu_func(int64_t i, int64_t k, matrix_t *a, scalar_t *akk_data, scalar_t *aik_data) {

      // std::cout << "[TrsmTask::cpu_func]  A(" << k << ", " << k <<  ") rank = " << a->tileRank(k, k) << std::endl;
      // std::cout << "[TrsmTask::cpu_func]  A(" << i << ", " << k <<  ") rank = " << a->tileRank(i, k) << std::endl;

      if (a->tileIsLocal(i, k)) {
         // Make sure diagonal tile has been created locally
         // assert(a->tileExists(k, k));
         
// #ifdef PARSEC_DTD_HAVE_CUDA
//          std::cout << "[PotrfSolveDataTask::cpu_func]  A(" << i << ", " << k <<  ")" << std::endl;
// #endif         
               
         auto aik = a->at(i, k);
         auto aik_stride = aik.stride();

         // Source Tile

         // auto akk = a->at(k, k);

         // A(k,k) tile does not exists locally so we need to create
         // it

         auto mb = a->tileMb(k);
         auto nb = a->tileNb(k);

         // If A(k, k) is local, then retrieve its stride from tile
         // object, otherwise its stride should be equal to the parsec
         // arena's leading dimensions.
         auto akk_stride = (a->tileIsLocal(k, k)) ? a->at(k, k).stride() : a->ld_arena;

         // std::cout << "[PotrfSolveDataTask::cpu_func] i = " << i << ", k = " << k << std::endl;
         // std::cout << "[PotrfSolveDataTask::cpu_func] akk, mb = " << mb << ", nb = " << nb << std::endl;
         // std::cout << "[PotrfSolveDataTask::cpu_func] akk_stride = " << akk_stride << std::endl;

         // if (a->tileIsLocal(k, k)) {
         //    std::cout << "[PotrfSolveDataTask::cpu_func] local akk, mb = " << a->at(k, k).mb() << ", nb = " << a->at(k, k).nb() << std::endl;
         // }
         
         slate::Tile<scalar_t> akk(
               mb, nb, akk_data, akk_stride, slate::HostNum, slate::TileKind::UserOwned, slate::Layout::ColMajor);
         akk.uplo(a->uplo());

         // std::cout << "[PotrfSolveDataTask::cpu_func]  mb = " << mb << std::endl;
         // std::cout << "[PotrfSolveDataTask::cpu_func]  nb = " << nb << std::endl;

         // std::cout << "[PotrfSolveDataTask::cpu_func]  akk stride = " << akk.stride() << std::endl;

         // std::cout << "[PotrfSolveDataTask::cpu_func]  akk tile data = " << akk.data() << std::endl;
         // std::cout << "[PotrfSolveDataTask::cpu_func]  akk_data = " << akk_data << std::endl;

         auto trans_akk = slate::conj_transpose(akk);

         slate::trsm<scalar_t>(blas::Side::Right, blas::Diag::NonUnit, scalar_t(1.0), trans_akk, aik);

         // Decrease source tile life counter  
         // A->tileTick(k, k);
      }
   }

   static int parsec_cpu_func(
         parsec_execution_stream_t *es, parsec_task_t *this_task) {

      (void)es;
         
      int64_t i;
      int64_t k;
      matrix_t *a;
      scalar_t *akk_data;
      scalar_t *aik_data;
      int tile_rank;
         
      parsec_dtd_unpack_args(this_task, &i, &k, &a, &akk_data, &aik_data, &tile_rank);

      // Note: Arena index associated with A(k,k) tile should always
      // be `parsec::FULL_TILE` as we run this task on the node
      // holding A(i,k) tile
      
      // Note: Arena index associated with A(i,k) should always be
      // `parsec::LAPACK_TILE` as we run this task on the node holding
      // A(i,k) tile
      
      cpu_func(i, k, a, akk_data, aik_data);

// #ifdef PARSEC_DTD_HAVE_CUDA
//       std::cout << "[PotrfSolveDataTask::parsec_cpu_func]"
//                 << " post aik = " << aik_data[0] << std::endl;
// #endif
      
      return PARSEC_HOOK_RETURN_DONE;      
   }

};
   
   //
   // PotrfSolve Parsec task
   //

template<typename matrix_t, typename scalar_t>
class PotrfSolveTask {
public:
   struct Arguments {
      // Tile row index
      int64_t i;
      // Tile column index
      int64_t k;
      // Matrix pointer
      matrix_t *a;

      // Default constructor
      Arguments() :
         i(-1), k(-1), a(nullptr) { }

      Arguments(int64_t in_i, int64_t in_k, matrix_t *in_a)
         : i(in_i), k(in_k), a(in_a) { }
   };

   // Run the task 
   void execute() {
      // TODO: check task has been initialized e.g. a != 0
      this->cpu_func(this->args_.i, this->args_.k, this->args_.a);
   }

   void initialize(Arguments const& in_args) {
      this->args_ = Arguments(in_args);
   }

   // Insert task
   void submit(parsec_taskpool_t *tp, int priority = 0) {

      auto i = args_.i;
      auto k = args_.k;
      // Retrieve rank owning tile a(i, k)
      int tile_rank = this->args_.a->tileRank(i, k);

      parsec_dtd_taskpool_insert_task(
            tp,
            parsec_cpu_func,
            priority, /* inherits the priority from high level task */
            "PotrfSolveTask",
            sizeof(decltype(i)), &i, VALUE,
            sizeof(decltype(k)), &k, VALUE,
            sizeof(matrix_t *), &args_.a, VALUE,
            sizeof(int), &tile_rank, VALUE | AFFINITY,
            PARSEC_DTD_ARG_END);
         
   }
      
private:

   Arguments args_;

   static void cpu_func(int64_t i, int64_t k, matrix_t *a) {

      // std::cout << "[TrsmTask::cpu_func]  A(" << k << ", " << k <<  ") rank = " << a->tileRank(k, k) << std::endl;
      // std::cout << "[TrsmTask::cpu_func]  A(" << i << ", " << k <<  ") rank = " << a->tileRank(i, k) << std::endl;

      if (a->tileIsLocal(i, k)) {

         // std::cout << "[PotrfSolveTask::cpu_func]  A(" << i << ", " << k <<  ")" << std::endl;

         // Make sure diagonal tile has been created locally
         assert(a->tileExists(k, k));
               
         auto aik = a->at(i, k);
         auto akk = a->at(k, k);

         auto trans_akk = slate::conj_transpose(akk);

         slate::trsm<scalar_t>(blas::Side::Right, blas::Diag::NonUnit, scalar_t(1.0), trans_akk, aik);

         // Decrease source tile life counter  
         // A->tileTick(k, k);
      }
   }

   static int parsec_cpu_func(
         parsec_execution_stream_t *es, parsec_task_t *this_task) {

      (void)es;
         
      int64_t i;
      int64_t k;
      matrix_t *a;
      int tile_rank;
         
      parsec_dtd_unpack_args(this_task, &i, &k, &a, &tile_rank);
         
      cpu_func(i, k, a);
         
      return PARSEC_HOOK_RETURN_DONE;      
   }

};

}

#endif POTRF_SOLVE_TASK_HH
