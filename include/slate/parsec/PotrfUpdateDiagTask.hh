#ifndef POTRF_UPDATE_DIAG_TASK_HH
#define POTRF_UPDATE_DIAG_TASK_HH

#include <iostream>

#include "internal/internal.hh"
#include "internal/internal_cublas.hh"
#include "slate/parsec/tasks.hh"

namespace parsec {

#ifdef PARSEC_DTD_HAVE_CUDA

template<typename matrix_t, typename scalar_t>
class PotrfUpdateDiagGpuTask {
public:
   struct Arguments {
      // Source row/column tile index
      int64_t k;
      // Destination row/column tile index
      int64_t j;
      // Matrix pointer
      matrix_t *a;
      // Push out data to main memory
      bool pushout;

      // Default constructor
      Arguments() :
         k(-1), j(-1), a(nullptr), pushout(false) { }

      Arguments(int64_t in_k, int64_t in_j, matrix_t *in_a)
         : k(in_k), j(in_j), a(in_a), pushout(false) { }
   };

   PotrfUpdateDiagGpuTask() { }

   void initialize(Arguments const& in_args) {
      this->args_ = Arguments(in_args);
   }

   // Insert task
   void submit(parsec_taskpool_t *tp, int priority = 0) {

      auto *a = args_.a;
      auto k = args_.k;
      auto j = args_.j;
      // Retrieve rank owning tile a(j, j)
      int tile_rank = this->args_.a->tileRank(j, j);

      // A(j, k) tile
      auto* ajk_dtd_tile = a->desc->dtd_tiles[a->desc->super.data_key(&(a->desc->super), j, k)];
      // int ajk_arena_index = (a->tileIsLocal(j, k)) ? parsec::LAPACK_TILE : parsec::FULL_TILE;
      int ajk_arena_index = parsec::FULL_TILE;
      
      // A(j, j) tile
      auto* ajj_dtd_tile = a->desc->dtd_tiles[a->desc->super.data_key(&(a->desc->super), j, j)];
      // int ajj_arena_index = (a->tileIsLocal(j, j)) ? parsec::LAPACK_TILE : parsec::FULL_TILE;
      int ajj_arena_index = parsec::FULL_TILE;

      int ajj_op_type = INOUT | ajj_arena_index;
      // If udpate takes places in block column right after the panel,
      // then we send the data back to the CPU after performing the
      // update.

      if (args_.pushout) {
         ajj_op_type |= PUSHOUT;
      }

      if (a->tileIsLocal(j, j)) {
         int cuda_dev_index = parsec::get_tile_device_index(a, j, j);

         // std::cout << "[PotrfUpdateDiagGpuTask::gpu_func]  k = " << k
         //           << ", A(" << j << ", " << j <<  ")"
         //           << ", device index = " << parsec::get_tile_device_index(a, j, j)
         //           << std::endl;
         
         parsec_data_copy_t *data_copy = ajj_dtd_tile->data_copy;
         assert(NULL != data_copy);
         parsec_data_t *data = data_copy->original;
         assert(NULL != data);
         parsec_advise_data_on_device(
               data, cuda_dev_index, PARSEC_DEV_DATA_ADVICE_PREFERRED_DEVICE);

      }
      
      parsec_dtd_taskpool_insert_cuda_task(
            tp,
            parsec_gpu_func,
            priority, /* inherits the priority from high level task */
            "PotrfUpdateDiagGpuTask",
            sizeof(decltype(k)), &k, VALUE,
            sizeof(decltype(j)), &j, VALUE,
            sizeof(matrix_t *), &args_.a, VALUE,
            PASSED_BY_REF, ajk_dtd_tile, INPUT | ajk_arena_index,
            PASSED_BY_REF, ajj_dtd_tile, ajj_op_type,
            sizeof(int), &tile_rank, VALUE | AFFINITY,
            PARSEC_DTD_ARG_END);
         
   }

   
private:

   Arguments args_;

   static void gpu_func(
         cublasHandle_t cuhandle, cudaStream_t cuda_stream,
         int64_t k, int64_t j, matrix_t *a,
         scalar_t *dev_ajk_data, scalar_t *dev_ajj_data) {

      if (a->tileIsLocal(j, j)) {

         int devid;
         cudaError_t err = cudaGetDevice(&devid);
  
         // std::cout << "[PotrfUpdateDiagGpuTask::gpu_func]  k = " << k
         //           << ", A(" << j << ", " << j <<  ")"
         //           << ", devid = " << devid
         //           << std::endl;

         auto ajj = a->at(j, j);

         auto alpha = blas::real_type<scalar_t>(-1.0);
         auto beta = blas::real_type<scalar_t>( 1.0);

         auto ajk_mb = a->tileMb(j);
         auto ajk_nb = a->tileNb(k);

         // auto ajk_stride = (a->tileIsLocal(j, k)) ? a->at(j, k).stride() : ajk_mb;

         // std::cout << "[PotrfUpdateDiagGpuTask::gpu_func] ajj_mb = " << ajj.mb()
         //           << ", ajj_nb = " << ajj.nb() << std::endl;

         // std::cout << "[PotrfUpdateDiagGpuTask::gpu_func] ajk_mb = " << ajk_mb
         //           << ", ajk_nb = " << ajk_nb << std::endl;

         // std::cout << "[PotrfUpdateDiagGpuTask::gpu_func] ajj_stride = " << ajj.stride()
         //           << ", ajk_stride = " << ajk_stride << std::endl;

         slate_cublas_call(
               slate::internal::cublasHerk(
                     cuhandle,  // uses stream
                     slate::internal::cublas_uplo_const(ajj.uploPhysical()),
                     CUBLAS_OP_N,
                     ajj.mb(), ajk_nb,
                     &alpha, dev_ajk_data, ajk_mb,
                     &beta,  dev_ajj_data, ajj.stride()));

         // slate_cuda_call(
         //       cudaStreamSynchronize(cuda_stream));

         // slate_cuda_call(
         //       cudaDeviceSynchronize());

      }      
   }
      
   static int parsec_gpu_func(
         void *gpu_device,
         cudaStream_t cuda_stream, parsec_task_t *this_task) {
         
      int64_t k;
      int64_t j;
      matrix_t *a;
      scalar_t *ajk_data;
      scalar_t *ajj_data;
      int tile_rank;
         
      parsec_dtd_unpack_args(this_task, &k, &j, &a, &ajk_data, &ajj_data, &tile_rank);

      scalar_t *dev_ajk_data = (scalar_t *)parsec_dtd_get_dev_ptr(this_task, 0);
      scalar_t *dev_ajj_data = (scalar_t *)parsec_dtd_get_dev_ptr(this_task, 1);

      // std::cout << "[PotrfUpdateDiagGpuTask::cpu_func] k = " << k << ", j = " << j << std::endl;
      // std::cout << "[PotrfUpdateDiagGpuTask::cpu_func] dev_ajk_data = " << dev_ajk_data
      //           << ", dev_ajj_data = " << dev_ajj_data << std::endl;

      // // DEBUG
      // scalar_t x, y;
      // cudaMemcpyAsync(
      //    &x, dev_ajk_data, sizeof(scalar_t),
      //    cudaMemcpyDeviceToHost, cuda_stream);   
      // cudaMemcpyAsync(
      //    &y, dev_ajj_data, sizeof(scalar_t),
      //    cudaMemcpyDeviceToHost, cuda_stream);   
      // cudaStreamSynchronize(cuda_stream);

      // printf("[PotrfUpdateDiagGpuTask::parsec_gpu_func] ajk = %.12f, ajj = %.12f\n", x, y);

      parsec_device_cuda_module_t *cuda_device = (parsec_device_cuda_module_t *)gpu_device;

      cublasStatus_t status;
      cublasHandle_t cuhandle = NULL;

      cuhandle = parsec::get_cublas_handle(cuda_stream);

      // status = cublasCreate(&cuhandle);
      // status = cublasSetStream(cuhandle, cuda_stream);
      
      // cuhandle = parsec::create_or_lookup_cublas_handle(
      //       &cuda_device->super, parsec::CuHI, cuda_stream);
      assert(NULL != cuhandle);
      
      gpu_func(
            cuhandle, cuda_stream,
            k, j, a,
            dev_ajk_data, dev_ajj_data);

      // scalar_t z;
      // cudaMemcpyAsync(
      //       &z, dev_ajj_data, sizeof(scalar_t),
      //       cudaMemcpyDeviceToHost, cuda_stream);   
      // cudaStreamSynchronize(cuda_stream);

      // printf("[PotrfUpdateDiagGpuTask::parsec_gpu_func] post ajj = %.12f\n", z);

      return PARSEC_HOOK_RETURN_DONE;
   }
   
   
};

#endif   
   
//
// PotrfUpdateDiag Parsec task with data dependency tracking
//

template<typename matrix_t, typename scalar_t>
class PotrfUpdateDiagDataTask {
public:
   struct Arguments {
      // Source row/column tile index
      int64_t k;
      // Destination row/column tile index
      int64_t j;
      // Matrix pointer
      matrix_t *a;

      // Default constructor
      Arguments() :
         k(-1), j(-1), a(nullptr) { }

      Arguments(int64_t in_k, int64_t in_j, matrix_t *in_a)
         : k(in_k), j(in_j), a(in_a) { }
   };

   PotrfUpdateDiagDataTask() { }
      
   // Run the task 
   // void execute() {
   //    // TODO: check task has been initialized e.g. a != 0
   //    this->cpu_func(this->args_.k, this->args_.j, this->args_.a);
   // }

   void initialize(Arguments const& in_args) {
      this->args_ = Arguments(in_args);
   }

   // Insert task
   void submit(parsec_taskpool_t *tp, int priority = 0) {

      auto *a = args_.a;
      auto k = args_.k;
      auto j = args_.j;
      // Retrieve rank owning tile a(j, j)
      int tile_rank = this->args_.a->tileRank(j, j);

      // A(j, k) tile
      auto* ajk_dtd_tile = a->desc->dtd_tiles[a->desc->super.data_key(&(a->desc->super), j, k)];
      int ajk_arena_index = (a->tileRank(j, k) == a->mpiRank()) ? parsec::LAPACK_TILE : parsec::FULL_TILE;

      // A(j, j) tile
      auto* ajj_dtd_tile = a->desc->dtd_tiles[a->desc->super.data_key(&(a->desc->super), j, j)];
      int ajj_arena_index = (a->tileRank(j, j) == a->mpiRank()) ? parsec::LAPACK_TILE : parsec::FULL_TILE;

      parsec_dtd_taskpool_insert_task(
            tp,
            parsec_cpu_func,
            priority, /* inherits the priority from high level task */
            "PotrfUpdateDiagDataTask",
            sizeof(decltype(k)), &k, VALUE,
            sizeof(decltype(j)), &j, VALUE,
            sizeof(matrix_t *), &args_.a, VALUE,
            PASSED_BY_REF, ajk_dtd_tile, INPUT | ajk_arena_index /*| AFFINITY*/,
            PASSED_BY_REF, ajj_dtd_tile, INOUT | ajj_arena_index /*| AFFINITY*/,
            sizeof(int), &tile_rank, VALUE | AFFINITY,
            PARSEC_DTD_ARG_END);
         
         
   }

private:

   Arguments args_;

   static void cpu_func(int64_t k, int64_t j, matrix_t *a, scalar_t *ajk_data, scalar_t *ajj_data) {

      if (a->tileIsLocal(j, j)) {

         // std::cout << "[PotrfUpdateDiagDataTask::cpu_func] Src A(" << j << ", " << k <<  ")" << std::endl;
// #ifdef PARSEC_DTD_HAVE_CUDA
//          std::cout << "[PotrfUpdateDiagDataTask::cpu_func] k = " << k
//                    << ", A(" << j << ", " << j <<  ")" << std::endl;
// #endif
         
         // Destination tile (local)
         auto ajj = a->at(j, j);
         auto ajj_stride = ajj.stride();

         // Source tile

         // auto ajk = a->at(j, k);

         // A(j,k) tile does not exists locally so we need to create
         // it using the remote data.

         auto mb = a->tileMb(j);
         auto nb = a->tileNb(k);

         // If A(j, k) is local, then retrieve its stride from tile
         // object, otherwise its stride should be equal to the parsec
         // arena's leading dimensions.
         auto ajk_stride = (a->tileIsLocal(j, k)) ? a->at(j, k).stride() : a->ld_arena;
         // ajk_stride = nb;
            
         // std::cout << "[PotrfUpdateDiagDataTask::cpu_func] k = " << k << ", j = " << j << std::endl;
         // std::cout << "[PotrfUpdateDiagDataTask::cpu_func] A(j,k) rank= " << a->tileRank(j, k) << ", A(j,j) rank = " << a->tileRank(j, j) << std::endl;
         // std::cout << "[PotrfUpdateDiagDataTask::cpu_func] A(j,k) mb = " << mb << ", nb = " << nb << std::endl;
         // std::cout << "[PotrfUpdateDiagDataTask::cpu_func] A(j,k) stride = " << ajk_stride << std::endl;

         slate::Tile<scalar_t> ajk(
               mb, nb, ajk_data, ajk_stride, slate::HostNum,
               slate::TileKind::UserOwned, slate::Layout::ColMajor);

         slate::herk<scalar_t>(
               blas::real_type<scalar_t>(-1.0), ajk,
               blas::real_type<scalar_t>( 1.0), ajj);

      }
   }

   static int parsec_cpu_func(
         parsec_execution_stream_t *es, parsec_task_t *this_task) {

      (void)es;
         
      int64_t k;
      int64_t j;
      matrix_t *a;
      scalar_t *ajk_data;
      scalar_t *ajj_data;
      int tile_rank;
         
      parsec_dtd_unpack_args(this_task, &k, &j, &a, &ajk_data, &ajj_data, &tile_rank);

      cpu_func(k, j, a, ajk_data, ajj_data);
         
      return PARSEC_HOOK_RETURN_DONE;      
   }

};   

   //
   // Potrf update diagonal Parsec task
   //

template<typename matrix_t, typename scalar_t>
class PotrfUpdateDiagTask {
public:
   struct Arguments {
      // Source row/column tile index
      int64_t k;
      // Destination row/column tile index
      int64_t j;
      // Matrix pointer
      matrix_t *a;

      // Default constructor
      Arguments() :
         k(-1), j(-1), a(nullptr) { }

      Arguments(int64_t in_k, int64_t in_j, matrix_t *in_a)
         : k(in_k), j(in_j), a(in_a) { }
   };

   PotrfUpdateDiagTask() { }
      
   // Run the task 
   void execute() {
      // TODO: check task has been initialized e.g. a != 0
      this->cpu_func(this->args_.k, this->args_.j, this->args_.a);
   }

   void initialize(Arguments const& in_args) {
      this->args_ = Arguments(in_args);
   }

   // Insert task
   void submit(parsec_taskpool_t *tp, int priority = 0) {

      auto k = args_.k;
      auto j = args_.j;
      // Retrieve rank owning tile a(j, j)
      int tile_rank = this->args_.a->tileRank(j, j);

      parsec_dtd_taskpool_insert_task(
            tp,
            parsec_cpu_func,
            priority, /* inherits the priority from high level task */
            "PotrfUpdateDiagTask",
            sizeof(decltype(k)), &k, VALUE,
            sizeof(decltype(j)), &j, VALUE,
            sizeof(matrix_t *), &args_.a, VALUE,
            sizeof(int), &tile_rank, VALUE | AFFINITY,
            PARSEC_DTD_ARG_END);
         
         
   }
      
private:

   Arguments args_;

   static void cpu_func(int64_t k, int64_t j, matrix_t *a) {

      if (a->tileIsLocal(j, j)) {

         // std::cout << "[PotrfUpdateDiagTask::cpu_func] Src A(" << j << ", " << k <<  ")" << std::endl;
         // std::cout << "[PotrfUpdateDiagTask::cpu_func] Dst A(" << j << ", " << j <<  ")" << std::endl;

         // Make sure source tile has been created locally
         assert(a->tileExists(j, k));

         // Source tile
         auto ajk = a->at(j, k);
         // Destination tile
         auto ajj = a->at(j, j);

         slate::herk<scalar_t>(
               blas::real_type<scalar_t>(-1.0), ajk,
               blas::real_type<scalar_t>( 1.0), ajj);

         // Decrease source tile life counter  
         // A->tileTick(j, k);
      }
   }

   static int parsec_cpu_func(
         parsec_execution_stream_t *es, parsec_task_t *this_task) {

      (void)es;
         
      int64_t k;
      int64_t j;
      matrix_t *a;
      int tile_rank;
         
      parsec_dtd_unpack_args(this_task, &k, &j, &a, &tile_rank);
         
      cpu_func(k, j, a);
         
      return PARSEC_HOOK_RETURN_DONE;      
   }

};

   
} // End of namespace parsec

#endif POTRF_UPDATE_DIAG_TASK_HH
