#ifndef POTRF_UPDATE_HH
#define POTRF_UPDATE_HH

#include "slate/parsec/PotrfUpdateTask.hh"

#include <iostream>

namespace parsec {

template<typename matrix_t, typename scalar_t>
class PotrfUpdate {

public:
   struct Arguments {
      // Current column
      int64_t k;
      // Lookahead
      int64_t la;
      // Matrix pointer
      matrix_t *a;

      // Default constructor
      Arguments() :
         k(-1), la(-1), a(nullptr) { }

      Arguments(int64_t in_k, int64_t in_la, matrix_t *in_a)
         : k(in_k), la(in_la), a(in_a) { }
   };
      
   PotrfUpdate() { }
   
   void initialize(Arguments const& in_args) {
      this->args_ = Arguments(in_args);
   }

   void submit(parsec_taskpool_t *tp, int priority = 0) {

      int64_t const k = this->args_.k;
      int64_t const lookahead = this->args_.la;
      matrix_t *a = this->args_.a;
   
      int64_t const A_nt = a->nt();

      for (int64_t j = k+1+lookahead; j < A_nt; ++j) {

         //
         // Update diagonal tile in column `j`
            
         // Task type
         using UpdateDiagDataTask = parsec::PotrfUpdateDiagDataTask<matrix_t, scalar_t>; 
         // Task arguments
         typename UpdateDiagDataTask::Arguments upd_diag_args(k, j, a);

         UpdateDiagDataTask upd_diag_task;
         upd_diag_task.initialize(upd_diag_args);
            
         // upd_diag_task.execute(); // Synchronous execution

         int priority = 2*A_nt - 2*k - j;
         upd_diag_task.submit(tp, priority); // Synchronous execution
         // parsec_dtd_taskpool_wait(A.parsec_context, tp);      

         //
         // Update sub-diagonal tile in column `j`

         for (int64_t i = j+1; i < A_nt; ++i) {

            // // Task type
            // using UpdateDataTask = parsec::PotrfUpdateDataTask<matrix_t, scalar_t>; 
            // // Task arguments
            // typename UpdateDataTask::Arguments upd_args(k, i, j, a);

            // UpdateDataTask upd_task;
            // upd_task.initialize(upd_args);

            // // upd_task.execute(); // Synchronous execution
            int priority = 2*A_nt - 2*k - i - j;
            // upd_task.submit(tp, priority); // Synchronous execution
            
            // Retrieve rank owning tile a(i, j)
            int tile_rank = a->tileRank(i, j);

            // A(i, k) tile
            auto* aik_dtd_tile = a->desc->dtd_tiles[a->desc->super.data_key(&(a->desc->super), i, k)];
            int aik_arena_index = (a->tileRank(i, k) == a->mpiRank()) ? parsec::LAPACK_TILE : parsec::FULL_TILE;

            // A(j, k) tile
            auto* ajk_dtd_tile = a->desc->dtd_tiles[a->desc->super.data_key(&(a->desc->super), j, k)];
            int ajk_arena_index = (a->tileRank(j, k) == a->mpiRank()) ? parsec::LAPACK_TILE : parsec::FULL_TILE;

            // A(i, j) tile
            auto* aij_dtd_tile = a->desc->dtd_tiles[a->desc->super.data_key(&(a->desc->super), i, j)];
            int aij_arena_index = (a->tileRank(i, j) == a->mpiRank()) ? parsec::LAPACK_TILE : parsec::FULL_TILE;

            parsec_dtd_taskpool_insert_task(
                  tp,
                  PotrfUpdateDataTask<matrix_t, scalar_t>::parsec_cpu_func,
                  priority, /* inherits the priority from high level task */
                  "PotrfUpdateTask",
                  sizeof(decltype(k)), &k, VALUE,
                  sizeof(decltype(j)), &i, VALUE,
                  sizeof(decltype(j)), &j, VALUE,
                  sizeof(matrix_t *), &args_.a, VALUE,
                  PASSED_BY_REF, aik_dtd_tile, INPUT | aik_arena_index /*| AFFINITY*/,
                  PASSED_BY_REF, ajk_dtd_tile, INPUT | ajk_arena_index /*| AFFINITY*/,
                  PASSED_BY_REF, aij_dtd_tile, INOUT | aij_arena_index /*| AFFINITY*/,
                  sizeof(int), &tile_rank, VALUE | AFFINITY,
                  PARSEC_DTD_ARG_END);

         }
      }
      
   }
   
private:

   Arguments args_;
   
};
   
}

#endif POTRF_UPDATE_HH
